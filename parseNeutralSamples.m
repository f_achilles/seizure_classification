function backgroundData = parseNeutralSamples(iNeutralSubj)
% The collector of bg data will have this shape:
% backgroundData{iNeutralSubj}{iShimGroup}{movement}{iSample}
% with dimensions {5}x{3}x{2}x{9} = 270

initializeShimmerClassification

% parameters
minDistanceBetweenSamples = 15*60*51.2; % = 15 minutes
numberOfMotionSamplesPerShimmer = 9;
numberOfNoMotionSamplesPerShimmer = 9;
sampleLength = 500;

test_eventID = iNeutralSubj;
networkDir = 'C:\Projects\SeizureClassification\Accelerometers\networks';
resultsDir = 'C:\Projects\SeizureClassification\Results';

% download updated .csv list from GoogleDocs
seizureList = GetGoogleSpreadsheet('1qnGrmK103yroKIf_kDUYDoZtFvxmRe62gq7LBMc_gHQ');
% page 2: 19wIL-j6geMGvy_pABonnsiyqWk2MTE56MnOU_AYhMfA/edit#gid=1018055961
seizureTable = cell2table(seizureList(2:end,:),'VariableNames',cell(seizureList(1,:)));
test_row = find(str2double(seizureTable{:,'event_ID'}) == test_eventID);

% check if the .csv files are in an unusual format
manualExport = false;
if strcmp('manualExport',seizureTable{test_row,'notes'})
    manualExport = true;
end

fullPatientPath = seizureTable{test_row,'filepath_shimmer'};
[patRoot,sourceFn,ext]=fileparts(fullPatientPath{1}); %patRoot = 'E:\Shimmer Data\exported_consensys\IM1307_2016-03-23_15.19.16_2016_03_23_SD_Session1';% patRoot = 'E:\Shimmer Data\IM1308_2016-03-24_16.16.31_2016_03_22_SD_Session2';
shimmerNames = seizureTable{test_row,...
    {'shimNameRARM', 'shimNameLARM', 'shimNameRLEG', 'shimNameLLEG', 'shimNameHIP'}...
    }; %{'1','2','4','5'}; %{'10a','12a','13a','14a'} for IM1308
invalidShimmers = strcmp(shimmerNames,'X');
shimmerNames(invalidShimmers) = [];
limbNames = {'rightWrist','leftWrist','rightAnkle','leftAnkle','abdomen'};
limbNames(invalidShimmers) = [];
sourceFn = [sourceFn ext];%sourceFn = '2016_03_23_Session1_Shimmer_%s_Calibrated_SD.csv'; %'2016_03_22_Session2_Shimmer_%s_Calibrated_SD.csv';
seizureStartTime = datenum(seizureTable{test_row,'clin_start'});
patientID = seizureTable{test_row,'Patient_ID'};%'IM1307';
patientID = patientID{1};
seizureID = sprintf('%02i',str2double(seizureTable{test_row,'sz_number'}));%'02';

mapping_from_TestShimID_to_TrainingShimName = zeros(numel(limbNames),1);
for iShim = 1:numel(shimmerNames)
    switch limbNames{iShim}
        case 'rightWrist'
            mapping_from_TestShimID_to_TrainingShimName(iShim) = 1;
        case 'leftWrist'
            mapping_from_TestShimID_to_TrainingShimName(iShim) = 1;
        case 'rightAnkle'
            mapping_from_TestShimID_to_TrainingShimName(iShim) = 5;
        case 'leftAnkle'
            mapping_from_TestShimID_to_TrainingShimName(iShim) = 7;
        case 'abdomen'
            mapping_from_TestShimID_to_TrainingShimName(iShim) = 9;
    end
end

if manualExport
    configFilePath = fullfile(patRoot,'Shimmers_Config.csv');
    confF = fopen(configFilePath);
    firstRow = fgetl(confF);
    timeoffset = zeros(1,numel(shimmerNames));
    configtime = timeoffset;
    for iShim = 1:numel(shimmerNames)
        tmp = fgetl(confF);
        dataRow = regexp(tmp,';','split');
        timeoffset(iShim) = str2double(dataRow{end});
        configtime(iShim) = str2double(dataRow{40});
    end
    fclose(confF);
end
    
wdw = 200;
testBatchSize = 60*51.2; %test 1 minute at once
beforeAfterSeiure = datenum(0,0,0,0,5,0); % report results for 5 minutes before and after the seizure

% Define the columns in the .CSV files
if manualExport
    % TS: [1]
    params.ts = 3;
    % Acc: [2-4]
    params.Acc = 11:13;
    % Gyro: [5-7]
    % Mag: [8-10]
    % Quat: [11-14]
    params.Quat = 4:7;
else
    % TS: [1]
    params.ts = 1;
    % Acc: [2-4]
    params.Acc = 2:4;
    % Gyro: [5-7]
    params.Gyro = 5:7;
    % Mag: [8-10]
    params.Mag= 8:10;
    % Quat: [11-14]
    params.Quat = 11:14;
end

backgroundData = cell(1,3);
backgroundData{1} = cell(1,2);
backgroundData{2} = cell(1,2);
backgroundData{3} = cell(1,2);

for iShim = 1:numel(shimmerNames)
    tic
%     % load network
%     load(fullfile(networkDir,...
%         sprintf('DNN_1DConv_3Layers_testSubj%d_shimmerNumber_%d_stretchAugWdw200Ratio02_lr_1e-2_2channels_momentum9_meanSdevNorm.mat',...
%         1,mapping_from_TestShimID_to_TrainingShimName(iShim))));
%     test_net = vl_simplenn_move(net,'gpu');
%     test_net.layers{end} = struct('type', 'softmax') ;

    predictions = single([]);
    timestamps = cell(1);
    features = cell(1);
    
    filename = sprintf(sourceFn,shimmerNames{iShim});
    fprintf('Reading %s ...\n',filename);
    shiF = fopen(fullfile(patRoot,filename));
    firstRow = fgetl(shiF); % gives away the data separator
    if ~manualExport
        secondRow = fgetl(shiF);
        dataTypes = regexp(secondRow,'\t','split');
        thirdRow = fgetl(shiF);
        unitNames = regexp(thirdRow,'\t','split');

        splitName = regexp(dataTypes{1},'_','split');
        params.sensorIDs = str2double(splitName{2});
    end

    % start datacrawling loop
    i = 1;
    initDone = false;
    jumpInMinutes = true;
    isFirstRow = true;
    testBatch = [];
    eofBeforeSeizure = false;
    UNIXmilliseconds = false;
    accMagZ = [];
    groundAngleZ = [];
    while 1
        try
            tmp = fgetl(shiF);
        catch e
            fprintf('Read error at Shimmer %s (%s)!\nprobably: broken file\n',...
                shimmerNames{iShim},limbNames{iShim});
            eofBeforeSeizure = true;
            break;
        end
        if ~ischar(tmp)
            fprintf('Read error at Shimmer %s (%s)!\nprobably: EoF\n',...
                shimmerNames{iShim},limbNames{iShim});
            eofBeforeSeizure = true;
            break;
        end
        
        dataRow = regexp(tmp,'\t','split');
        
        ts = datenum(dataRow{params.ts});
        
        if isFirstRow
            firstTs = ts;
            isFirstRow = false;
        end
        
        % compute features
        Q = str2double(dataRow(params.Quat))';
        acc = str2double(dataRow(params.Acc))';
        accInShimCoords = qRotatePoint(qRotatePoint(acc,Q) - [0;0;1],qInv(Q));
        yvec = qRotatePoint([0;1;0],Q);
        % collect features
        accMagZ(i) = abs(accInShimCoords(2))*9.81;
        groundAngleZ(i) = 180*acos(abs(yvec(3)))/pi;
        i = i+1;
        if i > 1*3600*51.2
            break;
        end
    end
    i = i-1;
    fclose(shiF);
    fprintf('Done reading after %f seconds.\nSelecting samples...\n',toc);
    % select "numberOfMotionSamplesPerShimmer" instances of 500
    % consectutive samples from the data
    accMagZ = accMagZ(1:(end-mod(i,sampleLength)));  %cropping
    groundAngleZ = groundAngleZ(1:(end-mod(i,sampleLength)));
    accMagZ = reshape(accMagZ,sampleLength,[]);             %reshaping
    groundAngleZ = reshape(groundAngleZ,sampleLength,[]);
    var_accMagZ = var(accMagZ,0,1);                         %variance
%     idx_over50 =  find(var_accMagZ > max(var_accMagZ)/2);  %movement
%     idx_over50 = idx_over50(randperm(numel(idx_over50),numberOfMotionSamplesPerShimmer));
    [~,sortIdx] = sort(var_accMagZ,2,'descend');
    idx_over90 = sortIdx(1:ceil(numel(var_accMagZ)/10));
    idx_over90 = idx_over90(randperm(numel(idx_over90),numberOfMotionSamplesPerShimmer));
    idx_under10 = find(var_accMagZ < max(var_accMagZ)/10);%noMovement
    idx_under10 = idx_under10(randperm(numel(idx_under10),numberOfNoMotionSamplesPerShimmer));
    for i = 1:numberOfMotionSamplesPerShimmer
        sample_motion{i} = [accMagZ(:,idx_over90(i)) groundAngleZ(:,idx_over90(i))];
    end
    for i = 1:numberOfNoMotionSamplesPerShimmer
        sample_noMotion{i} = [accMagZ(:,idx_under10(i)) groundAngleZ(:,idx_under10(i))];
    end
    
    % sort the data into one of the 3 groups: arms/legs/hip
    switch limbNames{iShim}
        case 'rightWrist'
            cGroup = 1;
        case 'leftWrist'
            cGroup = 1;
        case 'rightAnkle'
            cGroup = 2;
        case 'leftAnkle'
            cGroup = 2;
        case 'abdomen'
            cGroup = 3;
    end
    
    backgroundData{cGroup}{1} = [backgroundData{cGroup}{1} sample_motion];
    backgroundData{cGroup}{2} = [backgroundData{cGroup}{2} sample_noMotion];
    fprintf('Done.\n');
%     figure;
%     subplot(211)
%     plot(predictions')
%     legend(seqTypes(relevant_sequences(seqPerRelevantShimmer{...
%         relevant_shimmer_indices==mapping_from_TestShimID_to_TrainingShimName(iShim)...
%         })));
%     % modify ticks, replace sample numbers by Timestamps
%     set(gca,'xtick',1:(5*51.2):size(predictions,2)) %locations
%     set(gca,'xticklabel',datestr(timestamps(1:(5*51.2):size(predictions,2)),13)) %labels
%     set(gca,'xticklabelrotation',90) %labels
%     title(['Classification on ' limbNames{iShim}])
%     subplot(212)
%         plot([features{:}]')
%     legend('accMagZ','groundAngleZ');
%     % modify ticks, replace sample numbers by Timestamps
%     set(gca,'xtick',1:(5*51.2):size(predictions,2)) %locations
%     set(gca,'xticklabel',datestr(timestamps(1:(5*51.2):size(predictions,2)),13)) %labels
%     set(gca,'xticklabelrotation',90) %labels
%     title(['Features of ' limbNames{iShim}])
%     
%     set(gcf,'position',[1,1,1920,916])
%     drawnow
% 
%     % save result
%     save(fullfile(resultsDir,...
%         sprintf('SemiologyClassification_patient_%s_seizure_%s_shimmer_%s_%s.mat',...
%         patientID,seizureID,shimmerNames{iShim},limbNames{iShim})),...
%         'predictions','timestamps','features');
%     print(fullfile(resultsDir,...
%         sprintf('SemiologyClassification_patient_%s_seizure_%s_shimmer_%s_%s.png',...
%         patientID,seizureID,shimmerNames{iShim},limbNames{iShim})),'-dpng');
% 
%     fprintf('\nTime to\nbrowse %d hours,\nclassify %d hour, %d minutes,\nand save the result: %s\n',...
%         str2double(datestr(datenum(timestamps{end})-firstTs,'HH')),...
%         str2double(datestr(beforeAfterSeiure*2,'HH')),str2double(datestr(beforeAfterSeiure*2,'MM')),...
%         datestr(datenum([0,0,0,0,0,toc]),13));
end

for i = 1:3
    if numel(backgroundData{i}{1}) > numberOfMotionSamplesPerShimmer
        backgroundData{i}{1} = backgroundData{i}{1}{...
            randperm(numel(backgroundData{i}{1}),numberOfMotionSamplesPerShimmer)};
    end
    if numel(backgroundData{i}{2}) > numberOfNoMotionSamplesPerShimmer
        backgroundData{i}{2} = backgroundData{i}{2}{...
            randperm(numel(backgroundData{i}{2}),numberOfNoMotionSamplesPerShimmer)};
    end
end

end