%% definitions
% clear all

% this script points to the root directory of the seizure simulation data
% of June2015
initializeShimmerClassification

%% create Shimmer dataset
% What does this script do?
% 1) Define paths to training data etc.
% 2) Define the Shimmers that can be grouped, e.g. both arms, both legs and
% the trunk. Define the desired classes for each of these groups.
% 3) For each class, collect an equal amount of samples to train on
% 4) Store the classes and samples in an IMDB struct
%
%% TODO:
% - Where do these data come from? --> Seizure Types come from Simulation,
% "background" class comes from patient data.
% - In order to have a good Test Set vs. Training Set separation
% (test on unseen subjects!), it would be good to draw the background
% class form non-seizure recordings (+of patients that were not recorded
% with Shimmers during a seizure, so they will not appear on the positive list).
% 

%% 1) Define paths to training data etc.
% paths and feature settings

%% 2) Define the Shimmers that can be grouped, e.g. both arms, both legs and
% the trunk. Define the desired classes for each of these groups.
% group mappings

%% 3) For each class, collect an equal amount of samples to train on

% load parsed data of Simulations June 2015
parsedShimmerDataPath = fullfile(root,'ShimmerData_exported_filtered_parsed','parsed_shimmer_data_shimmers_13579.mat');
if exist(parsedShimmerDataPath,'file')
    load(parsedShimmerDataPath)
    disp('parsed shimmer data loaded')
else
    error('no parsed Shimmer Simulation data found. run parseSimulationData.m first.')
end
% the loaded variable is of the shape:
% parsed_shimmer_data{iShim}{iSeq}{iSubj}{sheet}
% with dimensions {4}x{7}x{5}x{2} = 280

% load non-seizure Shimmer background data
try
    load(fullfile(root,sprintf('bgData_parsed_onlyMagZandAngle.mat')),'backgroundData');
    disp('background data loaded...')
catch
    % choose -5- subjects for that, so to be fair w.r.t. to 5 positive subjects
    for iNeutralSubj = 1:5
        backgroundData{iNeutralSubj} = parseNeutralSamples(iNeutralSubj);
    end
    save(fullfile(root,sprintf('bgData_parsed_onlyMagZandAngle.mat')),'backgroundData');
end

% How should we pick suitable background patches?
% We need an equal number of bg instances in which there is movement and in
% which there is no movement. However in their sum, the bg instances cannot
% imbalance the dataset too much w.r.t. the seizure samples.
%
% A proper choice would be to source as many bg instances as there are
% seizure instances in total. This way, a Binary Seizure?Yes/No training
% would be feasible, in case the multiclass task proves to be too
% difficult.
%
% The collector of bg data will have this shape:
% backgroundData{iNeutralSubj}{iShimGroup}{movement}{iSample}
% with dimensions {5}x{3}x{2}x{9} = 270
%
% The length of each instance will have the average length of the instances
% of the training data:
% avgInstLength = 0;
% for iShim = 1:4
%     for iSeq = 1:7
%         for iSubj = 1:5
%             for sheet = 1:2
%                 avgInstLength = avgInstLength + ...
%                     size(parsed_shimmer_data{iShim}{iSeq}{iSubj}{sheet},1);
%             end
%         end
%     end
% end
% avgInstLength = avgInstLength/280 % result: 494.3929 ~500

%% 3.5) What about Test Data?
% --> Cross-Validate with 4 subjects training seizure, 4 subjects training
% non-seiure, 1 subject validatiaon seizure, 1 subject validation
% non-seizure

%% 4) Store the classes and samples in an IMDB struct

% seqTypes = {
%     'VersionRight_So4RightStretched_GTKA','VersionLeft_So4LeftStretched_GTKA',...
%     'AutomatismsBothHands',...
%     'Hypermotor',...
%     'ClonicRightArm','ClonicLeftArm',...
%     'TonicBothSides'};

%% 4.1) first fill the seizure part of the IMDB
isample = zeros(1,3);
imdb_group1 = struct('images',[]);
imdb_group2 = struct('images',[]);
imdb_group3 = struct('images',[]);
for iShim = 1:size(parsed_shimmer_data,2)
    for iSeq = 1:size(parsed_shimmer_data{1},2)
        % define label, based on Shimmer location (iShim) and activity (iSeq)
        switch iShim
            case 1 %RArm
                cGroup = 1;
%                 switch iSeq
%                     case 1
%                         %VersionSo4Ipsi
%                         label = 3;
%                     case 2
%                         %GTKAIpsi
%                         label = 4;
%                     case 3
%                         %VersionSo4Contra
%                         label = 5;
%                     case 3
%                         %VersionSo4Contra
%                         label = 5;
%                     case 3
%                         %AutomatismHands
%                         label = 5;
%                     case 4
%                         %Hypermotor
%                         label = 2;
%                     case 5
%                         %ClonicArm
%                         label = 6;
%                     case 6
%                         %nothing
%                         continue;
%                     case 7
%                         %nothing
%                         continue;
%                 end
                label = sequenceNumberToLabel_RArm(iSeq);
            case 2 %LArm
                cGroup = 1;
%                 switch iSeq
%                     case 1
%                         %VersionGTKAIpsi
%                         label = 3;
%                     case 2
%                         %VersionGTKAContra
%                         label = 4;
%                     case 3
%                         %AutomatismHands
%                         label = 5;
%                     case 4
%                         %Hypermotor
%                         label = 2;
%                     case 5
%                         %ClonicArm
%                         label = 6;
%                     case 6
%                         %nothing
%                         continue;
%                     case 7
%                         %nothing
%                         continue;
%                 end
                label = sequenceNumberToLabel_LArm(iSeq);
            case 3 %RLeg
                cGroup = 2;
%                 switch iSeq
%                     case 1
%                         %VersionGTKAIpsi
%                         label = 3;
%                     case 2
%                         %VersionGTKAContra
%                         label = 4;
%                     case 3
%                         %AutomatismHands
%                         continue;
%                     case 4
%                         %Hypermotor
%                         label = 2;
%                     case 5
%                         %ClonicArm
%                         continue;
%                     case 6
%                         %nothing
%                         continue;
%                     case 7
%                         %nothing
%                         continue;
%                 end
                label = sequenceNumberToLabel_RLeg(iSeq);
            case 4 %LLeg
                cGroup = 2;
%                 switch iSeq
%                     case 1
%                         %VersionGTKAIpsi
%                         label = 4;
%                     case 2
%                         %VersionGTKAContra
%                         label = 3;
%                     case 3
%                         %AutomatismHands
%                         continue;
%                     case 4
%                         %Hypermotor
%                         label = 2;
%                     case 5
%                         %ClonicArm
%                         continue;
%                     case 6
%                         %nothing
%                         continue;
%                     case 7
%                         %nothing
%                         continue;
%                 end
                label = sequenceNumberToLabel_LLeg(iSeq);
            case 5 %Trunk
                cGroup = 3;
%                 switch iSeq
%                     case 1
%                         %VersionGTKAIpsi
%                         label = 4;
%                     case 2
%                         %VersionGTKAContra
%                         label = 3;
%                     case 3
%                         %AutomatismHands
%                         continue;
%                     case 4
%                         %Hypermotor
%                         label = 2;
%                     case 5
%                         %ClonicArm
%                         continue;
%                     case 6
%                         %nothing
%                         continue;
%                     case 7
%                         %nothing
%                         continue;
%                 end
                label = sequenceNumberToLabel_LHip(iSeq);
        end
        
        if isnan(label)
            continue;
        end
        
        for sheet = 1:2
            for iSubj = 1:5
                subjData = parsed_shimmer_data{iShim}{iSeq}{iSubj};
                if isempty(subjData)
                    warning(sprintf(...
                        'No data from Shimmer with ID [%d] for subject %d!',relevant_shimmer_indices(iShim),iSubj));
                    continue;
                end
                smpl = subjData{sheet};
                if isempty(smpl)
                    warning('Broken sample!')
                    break;
                end
                N = size(smpl,1);
                % Z axis is along the limbs, which corresponds to the
                % Shimmer3 local frame axis "Y".
                % R "axis" is the cylindric counterpart combining the
                % local Shimmer3 X and Z axis.
                %
                % Quaternion gives the rotation in the NED frame
                accMagZ = zeros(N,1);
%                 accMagR = zeros(N,1);
%                 rotRateZ = zeros(N,1);
%                 rotRateR = zeros(N,1);
                groundAngleZ = zeros(N,1);
                for i = 1:N
                    % rotate ACC X*Y*Z* data into lowNoiseACC Coordinate system
                    Q = smpl(i,1:4)';
                    accInShimCoords = qRotatePoint(qRotatePoint(smpl(i,8:10)',Q) - [0;0;9.81],qInv(Q));
                    accMagZ(i) = abs(accInShimCoords(2));
%                     accMagR(i) = norm(accInShimCoords) - accMagZ(i);

%                     rotRateInShimCoords = qRotatePoint(smpl(i,5:7)',qInv(Q));
%                     rotRateZ(i) = abs(rotRateInShimCoords(2));
%                     rotRateR(i) = norm(rotRateInShimCoords) - rotRateZ(i);

                    % we want to know the absolute angle of the local
                    % Y-axis versus ground
                    yvec = qRotatePoint([0;1;0],Q);
                    groundAngleZ(i) = 180*acos(abs(yvec(3)))/pi;
                end
                isample(cGroup) = isample(cGroup)+1
                switch cGroup
                    case 1
        %                 imdb.images.data{s} = cat(3,accMagZ,accMagR,rotRateZ,rotRateR,groundAngleZ);
                        imdb_group1.images.data{isample(cGroup)} = cat(3,accMagZ,groundAngleZ);
                        imdb_group1.images.labels(1,1,1,isample(cGroup)) = label;
                        imdb_group1.meta.info(isample(cGroup)).subj = iSubj;
                        imdb_group1.meta.info(isample(cGroup)).sheet = sheet;
                        imdb_group1.meta.info(isample(cGroup)).seqType = seqTypes{relevant_sequences(iSeq)};
                    case 2
        %                 imdb.images.data{s} = cat(3,accMagZ,accMagR,rotRateZ,rotRateR,groundAngleZ);
                        imdb_group2.images.data{isample(cGroup)} = cat(3,accMagZ,groundAngleZ);
                        imdb_group2.images.labels(1,1,1,isample(cGroup)) = label;
                        imdb_group2.meta.info(isample(cGroup)).subj = iSubj;
                        imdb_group2.meta.info(isample(cGroup)).sheet = sheet;
                        imdb_group2.meta.info(isample(cGroup)).seqType = seqTypes{relevant_sequences(iSeq)};
                    case 3
        %                 imdb.images.data{s} = cat(3,accMagZ,accMagR,rotRateZ,rotRateR,groundAngleZ);
                        imdb_group3.images.data{isample(cGroup)} = cat(3,accMagZ,groundAngleZ);
                        imdb_group3.images.labels(1,1,1,isample(cGroup)) = label;
                        imdb_group3.meta.info(isample(cGroup)).subj = iSubj;
                        imdb_group3.meta.info(isample(cGroup)).sheet = sheet;
                        imdb_group3.meta.info(isample(cGroup)).seqType = seqTypes{relevant_sequences(iSeq)};
                end
            end
        end
    end
end

%% 4.2) then the non-seizure part
% backgroundData{iNeutralSubj}{iShimGroup}{movement}{iSample}
% with dimensions {5}x{3}x{2}x{9} = 270
label = 1;
neutral_seqTypes = {'neutral_with_movement','neutral_no_movement'};
for iGroup = 1:3
    for iNeutralSubj = 1:5
        if isempty(backgroundData{iNeutralSubj}{iGroup})
            continue;
        end
        for movement = 1:2
            for smpl = 1:size(backgroundData{iNeutralSubj}{iGroup}{movement},2)
                isample(iGroup) = isample(iGroup)+1
                switch iGroup
                    case 1
                        if size(backgroundData{iNeutralSubj}{iGroup}{movement},2) == 2
                            imdb_group1.images.data{isample(iGroup)} = reshape(backgroundData{iNeutralSubj}{iGroup}{movement},[500 1 2]);
                        else
                            imdb_group1.images.data{isample(iGroup)} = reshape(backgroundData{iNeutralSubj}{iGroup}{movement}{smpl},[500 1 2]);
                        end
                        imdb_group1.images.labels(1,1,1,isample(iGroup)) = label;
                        imdb_group1.meta.info(isample(iGroup)).subj = iNeutralSubj+5;
                        imdb_group1.meta.info(isample(iGroup)).sheet = movement;
                        imdb_group1.meta.info(isample(iGroup)).seqType = neutral_seqTypes{movement};
                    case 2
                        if size(backgroundData{iNeutralSubj}{iGroup}{movement},2) == 2
                            imdb_group2.images.data{isample(iGroup)} = reshape(backgroundData{iNeutralSubj}{iGroup}{movement},[500 1 2]);
                        else
                            imdb_group2.images.data{isample(iGroup)} = reshape(backgroundData{iNeutralSubj}{iGroup}{movement}{smpl},[500 1 2]);
                        end
                        imdb_group2.images.labels(1,1,1,isample(iGroup)) = label;
                        imdb_group2.meta.info(isample(iGroup)).subj = iNeutralSubj+5;
                        imdb_group2.meta.info(isample(iGroup)).sheet = movement;
                        imdb_group2.meta.info(isample(iGroup)).seqType = neutral_seqTypes{movement};
                    case 3
                        if size(backgroundData{iNeutralSubj}{iGroup}{movement},2) == 2
                            imdb_group3.images.data{isample(iGroup)} = reshape(backgroundData{iNeutralSubj}{iGroup}{movement},[500 1 2]);
                        else
                            imdb_group3.images.data{isample(iGroup)} = reshape(backgroundData{iNeutralSubj}{iGroup}{movement}{smpl},[500 1 2]);
                        end
                        imdb_group3.images.labels(1,1,1,isample(iGroup)) = label;
                        imdb_group3.meta.info(isample(iGroup)).subj = iNeutralSubj+5;
                        imdb_group3.meta.info(isample(iGroup)).sheet = movement;
                        imdb_group3.meta.info(isample(iGroup)).seqType = neutral_seqTypes{movement};
                end
            end
        end
    end
end

% save imdb
imdb = imdb_group1;
save(fullfile(root,sprintf('imdb_balanced_group1_onlyMagZandAngle_separatedGTKA.mat')),'imdb');
imdb = imdb_group2;
save(fullfile(root,sprintf('imdb_balanced_group2_onlyMagZandAngle_separatedGTKA.mat')),'imdb');
imdb = imdb_group3;
save(fullfile(root,sprintf('imdb_balanced_group3_onlyMagZandAngle_separatedGTKA.mat')),'imdb');


