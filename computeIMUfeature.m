function [imu_feature_t] = computeIMUfeature(sample)
% rotate ACC X*Y*Z* data into lowNoiseACC Coordinate system
Q = sample(1:4)';

accInShimCoords = qRotatePoint(qRotatePoint(sample(8:10)',Q) - [0;0;9.81],qInv(Q));
accMagZ = abs(accInShimCoords(2));
% accMagR(i) = norm(accInShimCoords) - accMagZ(i);
% 
% rotRateInShimCoords = qRotatePoint(smpl(i,5:7)',qInv(Q));
% rotRateZ(i) = abs(rotRateInShimCoords(2));
% rotRateR(i) = norm(rotRateInShimCoords) - rotRateZ(i);

% we want to know the absolute angle of the local
% Y-axis versus ground
yvec = qRotatePoint([0;1;0],Q);
groundAngleZ = 180*acos(abs(yvec(3)))/pi;

imu_feature_t = cat(3,accMagZ,groundAngleZ);
end