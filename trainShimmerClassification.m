%% definitions
% clear all
initializeShimmerClassification

%% train network
% clearing is important for repeated trainings
clear opts
% opts.train.learningRate = [
%     ones(1,60)*0.005...
%     ones(1,100)*0.004...
%     ones(1,100)*0.002...
%     0.001
%     ];
opts.train.learningRate = [1e-2];
opts.train.gpus = 1;
opts.train.numEpochs = 1000 ;
opts.train.batchSize = 100 ;
opts.train.momentum = 0.9 ;
testSubj = 1;
numClasses = [8, 6, 6];
% numClasses = [2, 2, 2]; % this learns a binary classifier! (but is the IMDB prepared for that?)
for iGroup = 1:3
    opts.imdbPath = fullfile('C:\Projects\SeizureClassification\Training',sprintf(...
        'imdb_balanced_group%d_onlyMagZandAngle_separatedGTKA.mat',iGroup));
    for testSubj = 1:5
        [net_bn, info_bn] = ...
        train_MLP(opts,'expDir',...
            fullfile('C:\Projects\SeizureClassification\Training',sprintf(...
            'multiclass_CNN_1DConv_3Layers_testSubj%d_group%d_cropAugWdw200R_lr_1e-2_2channels_momentum9_meanSdevNorm',...
            testSubj,iGroup)),...
        'batchNormalization', false, 'validationSubject', testSubj, 'numOutputs', numClasses(iGroup));
    end
end

%% test cross-validated networks: Plot Confusion Matrix, 
groupNames = {'Wrists','Ankles','Hip'};
wdw = 200;
debug = false;
if debug
    debugFig = figure;
end
cm = cell(1);
for iGroup = 1:3
    load(fullfile('C:\Projects\SeizureClassification\Training',sprintf(...
        'imdb_balanced_group%d_onlyMagZandAngle_separatedGTKA.mat',iGroup)));
    N = size(imdb.images.data,2);
    cm{iGroup} = zeros(numClasses(iGroup));
    estimatedLabels = [];
    gtLabels = [];
    
    for testSubj = 1:5
        % load network at best average early-Stopping epoch
        load(fullfile('C:\Projects\SeizureClassification\Training',sprintf(...
            'multiclass_CNN_1DConv_3Layers_testSubj%d_group%d_cropAugWdw200R_lr_1e-2_2channels_momentum9_meanSdevNorm',...
            testSubj,iGroup),'net-epoch-1000.mat'));
        test_net = vl_simplenn_move(net,'gpu');
        test_net.layers{end} = struct('type', 'softmax') ;
        % set validation samples
        imdb.images.set = ones(1,N);
        imdb.images.set([imdb.meta.info.subj] == testSubj) = 2;
        % TODO: apply network on validation samples
        % in that, go through the full sequence in a convolutional sense,
        % store all results!
        % to imitate the 200 to 100 resizing, just half the teporal
        % resolution!
%         testInput = gpuArray(zeros(1,1,1,1,'single'));
   
        for sNr = find(imdb.images.set == 2)
            testInput = gpuArray(imdb.images.data{sNr});
            if size(testInput,1) < wdw
                testInputBatched = testInput;
            else
                testInputBatched = gpuArray(zeros(wdw,1,2,size(testInput,1)-wdw+1,'single'));
                for iB = 1:size(testInputBatched,4)
                    testInputBatched(:,:,:,iB) = ...
                        bsxfun(@rdivide,...
                            bsxfun(@minus,...
                            testInput(iB:(wdw+iB-1),1,:),...
                            test_net.meta.average),...
                        test_net.meta.sdev);
                end
            end
            gtLabel = imdb.images.labels(sNr);
            res = vl_simplenn(test_net,...
                single(imresize(testInputBatched,[floor(wdw/2) 1])),...
                [],[],'mode','test');
            prediction = squeeze(gather(res(end).x));
            [~,predClassInds] = max(prediction,[],1);
            % uncomment to remove neutral estimates
%             predClassInds(predClassInds==1) = [];
            estimatedLabels = [estimatedLabels predClassInds];
            gtLabels = [gtLabels ones(size(predClassInds))*gtLabel];
            if debug
                figure(debugFig)
                subplot(211)
                plot(prediction'); axis tight
                subplot(212)
                plot(ones(numel(predClassInds),1)*gtLabel,'r-','linewidth',4)
                hold on
                plot(predClassInds);
                hold off
                axis tight
                set(gca,'YTick',1:numClasses(iGroup))
                set(gca,'YTickLabel',labelTypes(1:numClasses(iGroup)))
                axdim = axis;
                axis([axdim(1:2) 0.5 numClasses(iGroup)+0.5]);
                pause
            end
        end
    end
    cm{iGroup} = confusionmat(gtLabels, estimatedLabels);
    gtLabels_b = zeros(max(gtLabels),size(gtLabels,2));
    estimatedLabels_b = zeros(max(gtLabels),size(estimatedLabels,2));
    for iC = 1:max(gtLabels)
        gtLabels_b(iC,gtLabels==iC) = 1;
        estimatedLabels_b(iC,estimatedLabels==iC) = 1;
    end
    figure;
    plotconfusion(gtLabels_b,estimatedLabels_b);
%     imagesc(cm{iGroup}); axis equal tight
    title(sprintf('Shimmer Group %d (%s)',iGroup,groupNames{iGroup}));
    drawnow
end


