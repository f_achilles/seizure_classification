%% main
% this file is no functional script, it just documents the order in which
% library scripts are typically applied to end up with a working
% classification tool

% 1)
parseSimulationData

% 2)
createDataset

% 3)
trainShimmerClassification

% 4)
testOnPatients

%% TODOs

%
% load Shimmer data --> #done
%
% load Kinect video -->
% - required for computing sensitivity and specificity
% - use playX scripts
% - use synchro GUI, but replace the skeleton with the acceleration curves
% - save the offsets
% -- get an example seizure!
%
% load EEG video    --> #has no timestamp!
% - required for review with Noachtar
% - how to sync? timestamp in video image is wrong!
% -- Xltek might embed some TS in the avi file? or the start time?
%

