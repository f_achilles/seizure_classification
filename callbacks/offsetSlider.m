function handles = offsetSlider(handles, hObject)
% load data from handles
shimDat = handles.shimData;
sortedTimeList = handles.sortedTimeList;
runningIndices = handles.runningIndices;
shimmerIndices = handles.shimmerIndices;
NSamples = handles.totalframesShim;
quaternionColumns = handles.quaternionColumns;
params = handles.paramsShim;
bodyModel = handles.bodyModel;
% determine numSample
numSample=max(1,floor(get(hObject,'Value')*1e3));

% create next Shimmer "frame"
shimmerIndVec = [];
shimmerQuaternionsTmp = [];
allAboard = false;
while ~allAboard
    newIdx = shimmerIndices(numSample);
    shimmerSeat = find(newIdx==shimmerIndVec);
    if isempty(shimmerSeat)
        shimmerIndVec = [shimmerIndVec newIdx];
        shimmerQuaternionsTmp = [shimmerQuaternionsTmp ...
            shimDat(runningIndices(numSample),quaternionColumns{newIdx})];
    else
        shimmerQuaternionsTmp(:,(1:4)+4*(shimmerSeat-1)) = ...
            shimDat(runningIndices(numSample),quaternionColumns{newIdx});
    end
        
    if numel(params.sensorIDs) == numel(shimmerIndVec)
        allAboard = true;
    end
    
    if numSample < NSamples
        numSample = numSample+1;
    else
        break;
    end
end
% handles.frameIndShim = numSample;

% extract quaternions
shimmerQuaternionsTmp   = reshape(shimmerQuaternionsTmp,4,[]);

%this reserves space for 13 quaternions, only if the Shimmer-Name with the
%highest nummer would be "Shimmer13". That does not really make sense.
%
% The reason for that is the way Quaternions are processed in
% showBodyModel().
%
% params.mapIDsToMonotonNumbering
shimmerQuaternions      = repmat([1 0 0 0]',1,params.numShimmers);
shimmerQuaternions(:,params.mapIDsToMonotonNumbering(params.sensorIDs(shimmerIndVec))) =...
    shimmerQuaternionsTmp;
shimmerQuaternions      = ...
    mat2cell(shimmerQuaternions,4,ones(1,size(shimmerQuaternions,2)));

% plot Shimmer skeleton
axes(handles.axes2)
if isfield(bodyModel,'initDone')
    [bodyModel] = showBodyModel(params, bodyModel, shimmerQuaternions, ~bodyModel.initDone);
else
    [bodyModel] = showBodyModel(params, bodyModel, shimmerQuaternions, 1);
end
cShimTime = sortedTimeList(numSample);
set(handles.shimmerTimeString,'String',['Shimmer time: ' datestr(cShimTime,13)]);
drawnow

handles.bodyModel = bodyModel;

end