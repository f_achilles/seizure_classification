function handles = playSlider(handles, hObject)
% display new Kinect frame
newind=floor(get(hObject,'Value'));
handles.frameInd=newind;
set(handles.cImageHandle,'CData',rot90(handles.data{handles.actIdx}(:,:,handles.frameInd)));
set(handles.curFrameText,'String',num2str(handles.frameInd));
% display corresponding Kinect timestamp
try
    cKinectTime = handles.timestamps{handles.actIdx}(handles.frameInd,1);
    set(handles.systemTimeString,'String',['system time: ' datestr(cKinectTime,13)]);
catch
    cKinectTime = datenum(num2str(uint64(handles.timestamps{handles.actIdx}(handles.frameInd,1))),'yyyymmddHHMMSSFFF');
    set(handles.systemTimeString,'String',['system time: ' datestr(cKinectTime,13)]);
end

% display corresponding Shimmer frame
handles = updateShimmerFigure(handles,cKinectTime);
end