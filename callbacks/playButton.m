function handles = playButton(handles, hObject)

frameInd = handles.frameInd;
numSample = handles.frameIndShim;
playStat = get(hObject,'Value');
if playStat && isfield(handles,'data') && isfield(handles,'shimData')
    %Buttons are deactivated during play
    set(handles.slider1, 'Enable','inactive');
    set(handles.slider2, 'Enable','inactive');
    
    shimDat = handles.shimData;
    sortedTimeList = handles.sortedTimeList;
    runningIndices = handles.runningIndices;
    shimmerIndices = handles.shimmerIndices;
    NSamples = handles.totalframesShim;
    quaternionColumns = handles.quaternionColumns;
    params = handles.paramsShim;
    bodyModel = handles.bodyModel;
    tic
    while numSample < NSamples && get(hObject,'Value')
        shimmerQuaternionsOld = repmat([1 0 0 0]',1,params.numShimmers);
        shimmerIndVec = [];
        shimmerQuaternionsTmp = [];
        while ~any(shimmerIndices(numSample)==shimmerIndVec)
            shimmerIndVec = [shimmerIndVec shimmerIndices(numSample)];
            shimmerQuaternionsTmp = [shimmerQuaternionsTmp shimDat(runningIndices(numSample),quaternionColumns{shimmerIndices(numSample)})];
            if numSample < NSamples
                numSample = numSample+1;
            else
                break;
            end
        end
                
        shimmerQuaternionsTmp   = reshape(shimmerQuaternionsTmp,4,[]);
        shimmerQuaternions      = shimmerQuaternionsOld;
        shimmerQuaternions(:,params.sensorIDs(shimmerIndVec)) =...
            shimmerQuaternionsTmp;
        shimmerQuaternions      = ...
            mat2cell(shimmerQuaternions,4,ones(1,size(shimmerQuaternions,2)));

        % plot skeleton
        axes(handles.axes2)
        [h,bodyModel] = showBodyModel(params, bodyModel, shimmerQuaternions);
        time = toc;
        tic
        cShimTime = sortedTimeList(numSample);
        set(h,'string',['Time: ' datestr(cShimTime,13) ', readout frequency: ' num2str(round(1/time)) 'Hz.'])
        drawnow
        if handles.frameInd<handles.totalframes
            % draw a new kinect frame if we passed the timestamp
            nextKinectTime = handles.timestamps{handles.actIdx}(handles.frameInd+1,1);
            %if nextKinectTime < cShimTime
                frameInd=frameInd+1;
                handles.frameInd = frameInd;
                set(handles.curFrameText,'String',num2str(frameInd));
                set(handles.slider1,'Value',frameInd);
                set(handles.cImageHandle,'CData',rot90(handles.data{handles.actIdx}(:,:,handles.frameInd)));
%               axes(handles.axes1)
%               drawnow;
            %end
        end
        % skip shimmer frames for faster replay
%         numSample = numSample+2*params.numShimmers;
        set(handles.slider2,'Value',numSample);
        handles.frameIndShim = numSample;
        guidata(hObject, handles);
    %     pause(0.01)
    % body model was updated, now plot the individual parts
    %     drawBodyParts(params, bodyModel);
    %     drawnow
    end
    set(hObject,'Value',0)
    set(handles.slider1, 'Enable','on');
    set(handles.slider2, 'Enable','on');

end

end