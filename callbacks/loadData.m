function handles = loadData(handles, hObject)

% load Kinect Files
[file_name, folder_name] = ...
    uigetfile('*.mat','Please select a *.mat file containing an annotated Kinect video');
handles.path = folder_name;
handles.file = fullfile(folder_name,file_name);
actContainer = [];
timestamp = [];
load(handles.file);
handles.data = actContainer;
handles.timestamps = timestamp;
% plot first Kinect frame
actIdx = 1;
axes(handles.axes1)
himage=imagesc(rot90(actContainer{actIdx}(:,:,1)));
colormap(hot(3500))
caxis([1200 3500])
axis image
handles.cImageHandle = himage;
handles.totalframes = size(timestamp{actIdx},1);
handles.actIdx = actIdx;
% prepare Kinect slider
handles.frameInd = 1;%frame index of the Kinect secquence
handles.nFinal=handles.totalframes;% Last frame selected
handles.nInitial=1; % Initial frame selected
%Slider properties
set(handles.slider1, 'Enable','on');
set(handles.slider1, 'Min', 1);
set(handles.slider1, 'Max', handles.totalframes);
set(handles.slider1, 'Value', 1);
set(handles.slider1, 'SliderStep', [1/(handles.totalframes-1) , 1/(handles.totalframes-1) ]);
% set(handles.slideIn, 'String',num2str(1));
% set(handles.slideFin,'String',num2str(handles.totalframes));

% load Shimmer files
[file_name, folder_name] = ...
    uigetfile('*.mat','Please select a *.mat file containing a Shimmer recording');
handles.shimPath = folder_name;
handles.shimFile = fullfile(folder_name,file_name);
handles.frameIndShim = 1;
load(handles.shimFile);
handles.shimData = shimDat;
handles.paramsShim = params;

bodyModel = createBodyModel(params);
handles.bodyModel = bodyModel;

% determine important shimmer data for later replay
[sortedTimeList,runningIndices,shimmerIndices,NSamples,quaternionColumns] = ...
    getShimRecData(shimDat,params,1,endTimes);
handles.sortedTimeList = sortedTimeList;
handles.runningIndices = runningIndices;
handles.shimmerIndices = shimmerIndices;
handles.totalframesShim = NSamples;
handles.quaternionColumns = quaternionColumns;

% get Shimmer plot axes into focus
axes(handles.axes2)
rotate3d(gca)

handles.frameIndShim = 1; % frame index of the Shimmer secquence
handles.nFinalShim = handles.totalframesShim; % Last frame selected
handles.nInitialShim = 1; % Initial frame selected
%Slider properties
set(handles.slider2, 'Enable','on');
set(handles.slider2, 'Min', 0);
set(handles.slider2, 'Max', handles.totalframesShim/1e3);% TODO: Check if the slider is limited to +-1,000,000
set(handles.slider2, 'Value', 1e-3/handles.totalframesShim);
set(handles.slider2, 'SliderStep', [1e-3/(handles.totalframesShim-1) , 1e-3/(handles.totalframesShim-1) ]);

% set(handles.slideIn2, 'String',num2str(1));
% set(handles.slideFin2,'String',num2str(handles.totalframes));

% draw initial shimmer skeleton
try
cKinectTime = datenum(num2str(uint64(handles.timestamps{handles.actIdx}(handles.frameInd,1))),'yyyymmddHHMMSSFFF');
catch
cKinectTime = handles.timestamps{handles.actIdx}(handles.frameInd,1);
end
set(handles.systemTimeString,'String',['system time: ' datestr(cKinectTime,13)]);
[~,closestFrameShim] = min(abs(handles.sortedTimeList-cKinectTime));
handles.frameIndShim = closestFrameShim;
set(handles.slider2,'Value',handles.frameIndShim/1e3);


dummy = matlab.ui.control.UIControl;
set(dummy,'Value',closestFrameShim/1e3);
handles = offsetSlider(handles, dummy);


end