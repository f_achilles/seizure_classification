function handles = updateShimmerFigure(handles,cKinectTime)

if ~exist('cKinectTime','var')
    try
        cKinectTime = datenum(num2str(uint64(handles.timestamps{handles.actIdx}(handles.frameInd,1))),'yyyymmddHHMMSSFFF');
    catch
        cKinectTime = handles.timestamps{handles.actIdx}(handles.frameInd,1);
    end
end

% move slider to matching time (offset corrected)
[~,closestFrameShim] = min(abs(handles.sortedTimeList-cKinectTime+handles.offset));
handles.frameIndShim = closestFrameShim;
set(handles.slider2,'Value',handles.frameIndShim/1e3);

% forward slider position to drawing function
dummy = matlab.ui.control.UIControl;
set(dummy,'Value',closestFrameShim/1e3);
handles = offsetSlider(handles, dummy);

end