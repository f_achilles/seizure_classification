function handles = loadShimmerData(handles, hObject)

handles.path_shimmer = uigetdir('E:\Shimmer Data\exported_consensys','Please select a folder containing exported Shimmer files (*.csv)');
ext='*.csv';
handles.listing_shimmer = dir(strcat(handles.path_shimmer,filesep,ext));
numShimmers = numel(handles.listing_shimmer);

manualExport = false; %TODO: add condition for when this is true

kinectStartTime = datenum(num2str(uint64(handles.tscur(1))),'yyyymmddHHMMSSFFF');
beforeAfterSeiure = datenum(0,0,0,0,5,0); % report results for 5 minutes before and after the seizure

% Define the columns in the .CSV files
if manualExport
    % TS: [1]
    params.ts = 3;
    % Acc: [2-4]
    params.Acc = 11:13;
    % Gyro: [5-7]
    % Mag: [8-10]
    % Quat: [11-14]
    params.Quat = 4:7;
else
    % TS: [1]
    params.ts = 1;
    % Acc: [2-4]
    params.Acc = 2:4;
    % Gyro: [5-7]
    % Mag: [8-10]
    % Quat: [11-14]
    params.Quat = 11:14;
end

for iShim = 1:numShimmers
    tic
    
    timestamps = cell(1);
    quaternions = cell(1);
    acc = cell(1);
    
    filename = handles.listing_shimmer(iShim).name;
    shiF = fopen(fullfile(handles.path_shimmer,filename));
    firstRow = fgetl(shiF); % gives away the data separator
    if ~manualExport
        secondRow = fgetl(shiF);
        dataTypes = regexp(secondRow,'\t','split');
        thirdRow = fgetl(shiF);
        unitNames = regexp(thirdRow,'\t','split');

        splitName = regexp(dataTypes{1},'_','split');
        params.sensorIDs = str2double(splitName{2});
    end

    i = 1;
    initDone = false;
    jumpInMinutes = true;
    isFirstRow = true;
    testBatch = [];
    eofBeforeSeizure = false;
    UNIXmilliseconds = false;
%     if strcmp('yes',seizureTable{test_row,'unixTSonly'})
%         UNIXmilliseconds = true;
%     end
    
    fprintf('Searching Shimmer file ''%s'' for the closest timestamp to Kinect...\n',...
        filename);
    while 1
        try
            if jumpInMinutes
                for iL = 1:(60*51.2) %1 minute steps
                    tmp = fgetl(shiF);
                end
            else
                tmp = fgetl(shiF);
            end
        catch e
            fprintf('Read error at %s!\nprobably: file end before seizure begin\n',...
                filename);
            eofBeforeSeizure = true;
            break;
        end
        
        if ~ischar(tmp)
            fprintf('Read error at %s!\nprobably: file end before seizure end\n',...
                filename);
            eofBeforeSeizure = true;
            break;
        end
        
        dataRow = regexp(tmp,'\t','split');
        
        if UNIXmilliseconds
            ts = datetime(str2double(dataRow{params.ts})/1000, 'ConvertFrom', 'posixtime');
            ts = datenum(ts);
            if isnan(ts)
                eofBeforeSeizure = true;
                break;
            end
        elseif manualExport
            dataRow = regexp(tmp,',','split');
            ts = datetime(str2double(dataRow{params.ts})/1000 + timeoffset(iShim)/1000 + configtime(iShim), 'ConvertFrom', 'posixtime');
            ts = datenum(ts);
        else
            ts = datenum(dataRow{params.ts});
        end
        if isFirstRow
            firstTs = ts;
            isFirstRow = false;
        end
        if ~initDone && jumpInMinutes
            %disp(datestr(ts))
        end
        if ts < kinectStartTime-beforeAfterSeiure, continue; end
        if ts > kinectStartTime+beforeAfterSeiure, break; end
              
        jumpInMinutes = false;
        
        quaternions{i} = str2double(dataRow(params.Quat))';
        acc{i} = str2double(dataRow(params.Acc))';
        timestamps{i} = datestr(ts);
        i = i+1;
    end
    fclose(shiF);
    if eofBeforeSeizure
        continue;
    end
    
    axes(handles.(sprintf('axes_shim%i',iShim)));
    hold on
    plot([acc{:}]')
    hold off
    title(['Acceleration of ' filename])
    drawnow

    fprintf('\nTime to\nbrowse %d hours,\nand plot %d hour, %d minutes: %s\n',...
        str2double(datestr(datenum(timestamps{end})-firstTs,'HH')),...
        str2double(datestr(beforeAfterSeiure*2,'HH')),str2double(datestr(beforeAfterSeiure*2,'MM')),...
        datestr(datenum([0,0,0,0,0,toc]),13));
    
    handles.data_shimmer{iShim}.quat = quaternions;
    handles.data_shimmer{iShim}.acc  = acc;
    handles.data_shimmer{iShim}.ts   = timestamps;
end

% % load Shimmer files
% [file_name, folder_name] = ...
%     uigetfile('*.mat','Please select a *.mat file containing a Shimmer recording');
% handles.shimPath = folder_name;
% handles.shimFile = fullfile(folder_name,file_name);
% handles.frameIndShim = 1;
% load(handles.shimFile);
% handles.shimData = shimDat;
% handles.paramsShim = params;
% 
% bodyModel = createBodyModel(params);
% handles.bodyModel = bodyModel;
% 
% % determine important shimmer data for later replay
% [sortedTimeList,runningIndices,shimmerIndices,NSamples,quaternionColumns] = ...
%     getShimRecData(shimDat,params,1,endTimes);
% handles.sortedTimeList = sortedTimeList;
% handles.runningIndices = runningIndices;
% handles.shimmerIndices = shimmerIndices;
% handles.totalframesShim = NSamples;
% handles.quaternionColumns = quaternionColumns;
% 
% % get Shimmer plot axes into focus
% axes(handles.axes2)
% rotate3d(gca)
% 
% handles.frameIndShim = 1; % frame index of the Shimmer secquence
% handles.nFinalShim = handles.totalframesShim; % Last frame selected
% handles.nInitialShim = 1; % Initial frame selected
% %Slider properties
% set(handles.slider2, 'Enable','on');
% set(handles.slider2, 'Min', 0);
% set(handles.slider2, 'Max', handles.totalframesShim/1e3);% TODO: Check if the slider is limited to +-1,000,000
% set(handles.slider2, 'Value', 1e-3/handles.totalframesShim);
% set(handles.slider2, 'SliderStep', [1e-3/(handles.totalframesShim-1) , 1e-3/(handles.totalframesShim-1) ]);
% 
% % set(handles.slideIn2, 'String',num2str(1));
% % set(handles.slideFin2,'String',num2str(handles.totalframes));
% 
% % draw initial shimmer skeleton
% try
% cKinectTime = datenum(num2str(uint64(handles.timestamps{handles.actIdx}(handles.frameInd,1))),'yyyymmddHHMMSSFFF');
% catch
% cKinectTime = handles.timestamps{handles.actIdx}(handles.frameInd,1);
% end
% set(handles.systemTimeString,'String',['system time: ' datestr(cKinectTime,13)]);
% [~,closestFrameShim] = min(abs(handles.sortedTimeList-cKinectTime));
% handles.frameIndShim = closestFrameShim;
% set(handles.slider2,'Value',handles.frameIndShim/1e3);
% 
% 
% dummy = matlab.ui.control.UIControl;
% set(dummy,'Value',closestFrameShim/1e3);
% handles = offsetSlider(handles, dummy);


end