function handles = loadKinectData(handles, hObject)

% load Kinect Files
handles.path_kinect = uigetdir(pwd,'Please select a folder containing Kinect IR video files (*.KIR)');

ext='*.kir';
handles.listing_kinect = dir(strcat(handles.path_kinect,filesep,ext));

[dataContainer, tscur] = playKIR(strcat(handles.path_kinect,filesep,handles.listing_kinect(1).name),0);
handles.data = dataContainer;
handles.tscur = tscur;

% plot first Kinect frame
axes(handles.axes1)
himage = imshow(rot90(histeq(dataContainer(:,:,1))),[]);
% himage = imagesc(rot90(dataContainer(:,:,1))); %?
handles.cImageHandle = himage;
%caxis([1200 3500])
axis image

% prepare Kinect slider
handles.frameInd = 1;%frame index of the Kinect secquence
%handles.nFinal=handles.totalframes;% Last frame selected
handles.nInitial=1; % Initial frame selected
%Slider properties
set(handles.slider1, 'Enable','on');
set(handles.slider1, 'Min', 1);
set(handles.slider1, 'Max', numel(handles.tscur));
set(handles.slider1, 'Value', 1);
set(handles.slider1, 'SliderStep', [1/(numel(handles.tscur)-1) , 1/(numel(handles.tscur)-1) ]);

% set(handles.slideIn, 'String',num2str(1));
% set(handles.slideFin,'String',num2str(handles.totalframes));

end