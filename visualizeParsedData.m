function visualizeParsedData(filename)
clear plotShimmerChunk

if ~exist('filename','var')
    filename = 'parsed_shimmer_data_shimmers_1579.mat';
end
% loads seqTypes, relevant_sequences and seqPerRelevantShimmer
rootDir = [];
seqTypes = [];
relevant_sequences = [];
seqPerRelevantShimmer = [];
shimmerAtLimb = [];
relevant_shimmer_indices = [];
shimmerIDtoLimbName = [];
initializeShimmerClassification;

parsedDataDir = 'ShimmerData_exported_filtered_parsed';

% dummy
parsed_shimmer_data = [];
load(fullfile(rootDir,parsedDataDir,filename));

modes = {'nosheet','sheet'};

plotShimmerChunk();

for iSubj = 1:numel(parsed_shimmer_data{1}{1})
    for iShim = 1:numel(parsed_shimmer_data)
        for iSeq = 1:numel(parsed_shimmer_data{1})
            for iSheet = 1:2
                cdata = parsed_shimmer_data{iShim}{iSeq}{iSubj}{iSheet};
                if isempty(parsed_shimmer_data{iShim}{iSeq}{iSubj}{iSheet})
                    error('whaaat')
                end
                plotShimmerChunk(cdata);
            end
        end
    end
end

function plotShimmerChunk(chunk)
persistent plotHandle;    
if ~exist('chunk','var')
    % init figure
    plotHandle.fig = figure;
    plotHandle.graph = plot(ones(10,2));
    return;
end
figure(plotHandle.fig);

% compute feature signal
N = size(chunk,1);
feature_signal = zeros(N,2);
for s = 1:N
    [imu_feature_t] = computeIMUfeature(chunk(s,:));
    feature_signal(s,:) = squeeze(imu_feature_t);
end
for featID = 1:2
    set(plotHandle.graph(featID),'ydata',feature_signal(:,featID));
end
title({sprintf(...
    'Subject %d, Limb: %s,',...
    iSubj, shimmerIDtoLimbName{relevant_shimmer_indices(iShim)});...
    sprintf('%s, %s.',seqTypes{relevant_sequences(iSeq)}, modes{iSheet})},...
    'Interpreter','none')
drawnow

pause
end

end

