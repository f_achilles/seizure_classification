function varargout = Kinect_Shimmer_Synchronization_GUI(varargin)
%KINECT_SHIMMER_SYNCHRONIZATION_GUI M-file for Kinect_Shimmer_Synchronization_GUI.fig
%      KINECT_SHIMMER_SYNCHRONIZATION_GUI, by itself, creates a new KINECT_SHIMMER_SYNCHRONIZATION_GUI or raises the existing
%      singleton*.
%
%      H = KINECT_SHIMMER_SYNCHRONIZATION_GUI returns the handle to a new KINECT_SHIMMER_SYNCHRONIZATION_GUI or the handle to
%      the existing singleton*.
%
%      KINECT_SHIMMER_SYNCHRONIZATION_GUI('Property','Value',...) creates a new KINECT_SHIMMER_SYNCHRONIZATION_GUI using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to Kinect_Shimmer_Synchronization_GUI_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      KINECT_SHIMMER_SYNCHRONIZATION_GUI('CALLBACK') and KINECT_SHIMMER_SYNCHRONIZATION_GUI('CALLBACK',hObject,...) call the
%      local function named CALLBACK in KINECT_SHIMMER_SYNCHRONIZATION_GUI.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Kinect_Shimmer_Synchronization_GUI

% Last Modified by GUIDE v2.5 06-Nov-2016 22:12:44

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Kinect_Shimmer_Synchronization_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @Kinect_Shimmer_Synchronization_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Kinect_Shimmer_Synchronization_GUI is made visible.
function Kinect_Shimmer_Synchronization_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

% Choose default command line output for Kinect_Shimmer_Synchronization_GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Kinect_Shimmer_Synchronization_GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Kinect_Shimmer_Synchronization_GUI_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles = playSlider(handles, hObject);
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in playKinbutton.
function playKinbutton_Callback(hObject, eventdata, handles)
% hObject    handle to playKinbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles = playButtonEasy(handles, hObject);
guidata(hObject, handles);


% --- Executes on slider movement.
function slider2_Callback(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(hObject,'Value',get(hObject,'Value')*handles.totalframesShim/1e3)
if get(hObject,'Value')>get(hObject,'Max')
    set(hObject,'Value',get(hObject,'Max'))
end
handles = offsetSlider(handles, hObject);
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function slider2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in loadShimmerButton.
function loadShimmerButton_Callback(hObject, eventdata, handles)
% hObject    handle to loadShimmerButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles = loadKinectData(handles, hObject);

handles = loadShimmerData(handles, hObject);

%handles = loadData(handles, hObject);

guidata(hObject, handles);

% --- Executes on selection change in actlist.
function actlist_Callback(hObject, eventdata, handles)
% hObject    handle to actlist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns actlist contents as cell array
%        contents{get(hObject,'Value')} returns selected item from actlist

handles.actIdx = get(hObject,'Value');
if isfield(handles,'timestamps')
handles.frameInd = 1;
handles.totalframes = size(handles.timestamps{handles.actIdx},1);

set(handles.curFrameText,'String',num2str(handles.frameInd));
set(handles.slider1,'Value',handles.frameInd);
set(handles.slider1, 'Max', handles.totalframes);
set(handles.slider1, 'SliderStep', [1/(handles.totalframes-1) , 1/(handles.totalframes-1) ]);
set(handles.cImageHandle,'CData',rot90(histeq(handles.data{handles.actIdx}(:,:,handles.frameInd))));

dummy = matlab.ui.control.UIControl;
set(dummy,'Value',handles.frameInd);
handles = playSlider(handles, dummy);
end

guidata(hObject,handles)


% --- Executes during object creation, after setting all properties.
function actlist_CreateFcn(hObject, eventdata, handles)
% hObject    handle to actlist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in loadtablebutton.
function loadtablebutton_Callback(hObject, eventdata, handles)
% hObject    handle to loadtablebutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[path, name]=fileparts(handles.shimFile);
fid=fopen(fullfile(path,[name '_offset.txt']));
readIn=textscan(fid,'%25f');
handles.offset=double(readIn{1});
fclose(fid);
if fid==-1
    error('No offset file found!')
end
[~,~,~,~,M,S]=datevec(abs(handles.offset));
if handles.offset >= 0
    set(handles.offsSlider,'Value',M+S/60)
else
    set(handles.offsSlider,'Value',-M-S/60)
end
offsSlider_Callback(handles.offsSlider,1,handles);
guidata(hObject,handles)

% --- Executes on button press in savebutton.
function savebutton_Callback(hObject, eventdata, handles)
% hObject    handle to savebutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[path, name]=fileparts(handles.shimFile);
fid=fopen(fullfile(path,[name '_offset.txt']),'w+');
fprintf(fid,'%5.21f\n',handles.offset);
fclose(fid);
guidata(hObject,handles)


function offsMinutes_Callback(hObject, eventdata, handles)
% hObject    handle to offsMinutes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

M = str2double(get(hObject,'String'));
S = abs(str2double(get(handles.offsSeconds,'String')));
S = S + abs(str2double(get(handles.offsMilliseconds,'String'))/1000);
if M >= 0
    % make all positive
    set(handles.offsSeconds,'String',num2str(floor(S)));
    set(handles.offsMilliseconds,'String',num2str(round((S-floor(S))*1000)));
else
    % make all negative
    set(handles.offsSeconds,'String',num2str(-floor(S)));
    set(handles.offsMilliseconds,'String',num2str(-round((S-floor(S))*1000)));
    S=-S;
end
handles.offset = datenum(0,0,0,0,M,S);
handles = updateShimmerFigure(handles);

% adjust slider
set(handles.offsSlider,'Value',M+S/60)

guidata(hObject, handles);
% Hints: get(hObject,'String') returns contents of offsMinutes as text
%        str2double(get(hObject,'String')) returns contents of offsMinutes as a double


% --- Executes during object creation, after setting all properties.
function offsMinutes_CreateFcn(hObject, eventdata, handles)
% hObject    handle to offsMinutes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function offsSeconds_Callback(hObject, eventdata, handles)
% hObject    handle to offsSeconds (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
S = str2double(get(hObject,'String'));
M = abs(str2double(get(handles.offsMinutes,'String')));
if S >= 0
    % make all positive
    S = S + abs(str2double(get(handles.offsMilliseconds,'String'))/1000);
    set(handles.offsMinutes,'String',num2str(M));
    set(handles.offsMilliseconds,'String',num2str(round((S-floor(S))*1000)));
else
    % make all negative
    S = abs(S);
    S = S + abs(str2double(get(handles.offsMilliseconds,'String'))/1000);
    set(handles.offsMinutes,'String',num2str(-M));
    set(handles.offsMilliseconds,'String',num2str(-round((S-floor(S))*1000)));
    S = -S;
    M = -M;
end
handles.offset = datenum(0,0,0,0,M,S);
handles = updateShimmerFigure(handles);

% adjust slider
set(handles.offsSlider,'Value',M+S/60)

guidata(hObject, handles);
% Hints: get(hObject,'String') returns contents of offsSeconds as text
%        str2double(get(hObject,'String')) returns contents of offsSeconds as a double


% --- Executes during object creation, after setting all properties.
function offsSeconds_CreateFcn(hObject, eventdata, handles)
% hObject    handle to offsSeconds (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function offsMilliseconds_Callback(hObject, eventdata, handles)
% hObject    handle to offsMilliseconds (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
MS = str2double(get(hObject,'String'));
M = abs(str2double(get(handles.offsMinutes,'String')));
S = abs(str2double(get(handles.offsSeconds,'String')));
if MS >= 0
    % make all positive
    set(handles.offsMinutes,'String',num2str(M));
    set(handles.offsSeconds,'String',num2str(S));
    S = S + MS/1000;
else
    % make all negative
    MS = abs(MS);
    set(handles.offsMinutes,'String',num2str(-M));
    set(handles.offsSeconds,'String',num2str(-S));
    S = S + MS/1000;
    S = -S;
    M = -M;
end
handles.offset = datenum(0,0,0,0,M,S);
handles = updateShimmerFigure(handles);

% adjust slider
set(handles.offsSlider,'Value',M+S/60)

guidata(hObject, handles);
% Hints: get(hObject,'String') returns contents of offsMilliseconds as text
%        str2double(get(hObject,'String')) returns contents of offsMilliseconds as a double


% --- Executes during object creation, after setting all properties.
function offsMilliseconds_CreateFcn(hObject, eventdata, handles)
% hObject    handle to offsMilliseconds (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function curFrameText_Callback(hObject, eventdata, handles)
% hObject    handle to curFrameText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of curFrameText as text
%        str2double(get(hObject,'String')) returns contents of curFrameText as a double


% --- Executes during object creation, after setting all properties.
function curFrameText_CreateFcn(hObject, eventdata, handles)
% hObject    handle to curFrameText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function totframes_Callback(hObject, eventdata, handles)
% hObject    handle to totframes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of totframes as text
%        str2double(get(hObject,'String')) returns contents of totframes as a double


% --- Executes during object creation, after setting all properties.
function totframes_CreateFcn(hObject, eventdata, handles)
% hObject    handle to totframes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function offsSlider_Callback(hObject, eventdata, handles)
% hObject    handle to offsSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% convert minutes to datenum
handles.offset = datenum(0,0,0,0,get(hObject,'Value'),0);
handles = updateShimmerFigure(handles);

[~,~,~,~,MI,S] = datevec(abs(handles.offset));
if handles.offset >= 0
    set(handles.offsMinutes,'String',num2str(MI));
    set(handles.offsSeconds,'String',num2str(floor(S)));
    set(handles.offsMilliseconds,'String',num2str(round((S-floor(S))*1000)));
else
    set(handles.offsMinutes,'String',num2str(-MI));
    set(handles.offsSeconds,'String',num2str(-floor(S)));
    set(handles.offsMilliseconds,'String',num2str(-round((S-floor(S))*1000)));
end

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function offsSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to offsSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

set(hObject,'Min',-5)
set(hObject,'Max',5)
set(hObject,'Value',0)

handles.offset = 0;

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
guidata(hObject, handles);



function offsetAmpl_Callback(hObject, eventdata, handles)
% hObject    handle to offsetAmpl (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.offsetAmpl = abs(str2double(get(hObject,'String')));
set(hObject,'String',num2str(handles.offsetAmpl));

set(handles.offsSlider,'Min',-handles.offsetAmpl)
set(handles.offsSlider,'Max',handles.offsetAmpl)

% Hints: get(hObject,'String') returns contents of offsetAmpl as text
%        str2double(get(hObject,'String')) returns contents of offsetAmpl as a double
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function offsetAmpl_CreateFcn(hObject, eventdata, handles)
% hObject    handle to offsetAmpl (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

handles.offsetAmpl = 5;
set(hObject,'String',num2str(handles.offsetAmpl));


% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
guidata(hObject, handles);


% --- Executes on button press in playShimmerButton.
function playShimmerButton_Callback(hObject, eventdata, handles)
% hObject    handle to playShimmerButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

skipShim = 40;

handles.frameIndShim = get(handles.slider2,'Value')+skipShim/1e3;

while get(hObject,'Value')
    dummy = matlab.ui.control.UIControl;
    set(dummy,'Value',handles.frameIndShim);
    try
        handles = offsetSlider(handles, dummy);
    catch maxShimmerSample
    end
    set(handles.slider2,'Value',handles.frameIndShim);
    handles.frameIndShim = handles.frameIndShim+skipShim/1e3;
end

% Hint: get(hObject,'Value') returns toggle state of playShimmerButton
guidata(hObject, handles);
