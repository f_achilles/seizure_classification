%% definitions
% clear all
initializeShimmerClassification

%% Parse filtered data, using Kinect timestamps
% Monday_61min 
shimmerFile = dir(fullfile(root,'Monday_61min_09h26m.mat'));
load(fullfile(root,shimmerFile.name));
iSubj = 0;
iCtr = 0;
offset = datenum(0,0,0,0,0,1.65); % 1,65 Sekunden
for subjects = {'Person1','Person2'}
    iSubj = iSubj+1;
    sheetyesno = 0;
    for suffix = {'nosheet','sheet'}
        sheetyesno = sheetyesno+1;
        fprintf('loading %s...\n',sprintf('SimulationMonday_%s_%s.mat',subjects{1},suffix{1}));
        tic
        load(fullfile(root,sprintf('SimulationMonday_%s_%s.mat',subjects{1},suffix{1})),'timestamp');
        toc
        for iShim = 1:numel(relevant_shimmer_indices)
            col_group_in_ShimDat = find(relevant_shimmer_indices(iShim) == params.sensorIDs);
            if ~isempty(col_group_in_ShimDat)
                for iSeq = 1:numel(relevant_sequences)
                    initTime = datenum(num2str(int64(timestamp{relevant_sequences(iSeq)}(1,1))),'yyyymmddHHMMSSFFF');
                    endTime = datenum(num2str(int64(timestamp{relevant_sequences(iSeq)}(end,1))),'yyyymmddHHMMSSFFF');
                    % find index closest to init and end time
                    [~,initIndex] = min(abs(shimDat(:,(col_group_in_ShimDat-1)*14+1)-initTime-offset));
                    [~,endIndex] = min(abs(shimDat(:,(col_group_in_ShimDat-1)*14+1)-endTime-offset));
                    parsed_shimmer_data{iShim}{iSeq}{iSubj}{sheetyesno} =...
                        single(shimDat(initIndex:endIndex,...
                        (col_group_in_ShimDat-1)*14+(2:14)));
                    iCtr = iCtr+1
%                     disp(endIndex - initIndex)
                    if isempty(parsed_shimmer_data{iShim}{iSeq}{iSubj}{sheetyesno})
                        error('whaaat')
                    end
                end
            end
        end
    end
end

% Parse Monday_150min_12h11m data, using Kinect timestamps
shimmerFile = dir(fullfile(root,'Monday_150min_12h11m.mat'));
load(fullfile(root,shimmerFile.name));
for subjects = {'Person3','Person4','Person5'}
    iSubj = iSubj+1;
    sheetyesno = 0;
    for suffix = {'nosheet','sheet'}
        sheetyesno = sheetyesno+1;
        fprintf('loading %s...\n',sprintf('SimulationMonday_%s_%s.mat',subjects{1},suffix{1}));
        tic
        load(fullfile(root,sprintf('SimulationMonday_%s_%s.mat',subjects{1},suffix{1})),'timestamp');
        toc
        for iShim = 1:numel(relevant_shimmer_indices)
            col_group_in_ShimDat = find(relevant_shimmer_indices(iShim) == params.sensorIDs);
            if ~isempty(col_group_in_ShimDat)
                for iSeq = 1:numel(relevant_sequences)
                    initTime = datenum(num2str(int64(timestamp{relevant_sequences(iSeq)}(1,1))),'yyyymmddHHMMSSFFF');
                    endTime = datenum(num2str(int64(timestamp{relevant_sequences(iSeq)}(end,1))),'yyyymmddHHMMSSFFF');
                    % find index closest to init and end time
                    [~,initIndex] = min(abs(shimDat(:,(col_group_in_ShimDat-1)*14+1)-initTime-offset));
                    [~,endIndex] = min(abs(shimDat(:,(col_group_in_ShimDat-1)*14+1)-endTime-offset));
                    parsed_shimmer_data{iShim}{iSeq}{iSubj}{sheetyesno} =...
                        single(shimDat(initIndex:endIndex,...
                        (col_group_in_ShimDat-1)*14+(2:14)));
                    iCtr = iCtr+1
%                     disp(endIndex - initIndex)
                    if isempty(parsed_shimmer_data{iShim}{iSeq}{iSubj}{sheetyesno})
                        error('whaaat')
                    end
                end
            end
        end
    end
end

%% Save parsed Shimmer data to disk
save(fullfile(root,'parsed_shimmer_data_shimmers_1579.mat'),'parsed_shimmer_data');

%% make one big IMDB
% load(fullfile(root,'parsed_shimmer_data.mat'));
% imdb = struct('images',[]);
% wdwL = 100;
% delta = 25;
% s = 0;
% for iShim = 1:4
%     for iSeq = 1:7
%         for sheet = 1:2
%             iwdw = 0;
%             wdwV = true;
%             while wdwV
%                 iwdw = iwdw + 1;
%                 tmp = [];
%                 for iSubj = 1:5
%                     smpl = parsed_shimmer_data{iShim}{iSeq}{iSubj}{sheet};
%                     s = s+1
%                     imdb.images.data(1,1,:,s)   = tmp;
%                     imdb.images.labels(1,1,1,s) = iSeq;
%                     imdb.meta.info(s).subj      = iSubj;
%                     imdb.meta.info(s).sheet     = sheet;
%                     
%                     if isempty(smpl)
%                         wdwV = false;
%                         warning('Broken sample!')
%                         break;
%                     end
%                     if size(smpl,1) >= (iwdw-1)*delta+wdwL
%                         tmp = cat(3,tmp,reshape(smpl((iwdw-1)*delta + (1:wdwL),1:4)',1,1,[]));
%                     else
%                         wdwV = false;
%                         break;
%                     end
%                 end
% %                 if wdwV
% % 
% %                 end
%             end
%         end
%     end
% end
%% save big IMDB
% save(fullfile(root,'parsed_shimmer_imdb.mat'),'imdb');

%% create one IMDB per shimmer

% indices in shimmer_data:
% [1-4]: Quat
% [5-7]: Gyro
% [8-10]: Acc
% [11-13]: Magnetic

% seqTypes = {'VersionRight_So4RightStretched_GTKA','VersionLeft_So4LeftStretched_GTKA',...
%     'AutomatismsBothHands','OralAutomatisms','Hypermotor',...
%     'ClonicRightArm','ClonicLeftArm','TonicBothSides'};
% % all except oral automatisms
% relevant_sequences = [1 2 3 5 6 7 8];
for iShim = 1:4
    s = 0;
    imdb = struct('images',[]);
    for iSeq = 1:7
        label = find(seqPerRelevantShimmer{iShim} == iSeq);
        if isempty(label)
            continue
        end
        for sheet = 1:2
            for iSubj = 1:5
                smpl = parsed_shimmer_data{iShim}{iSeq}{iSubj}{sheet};
                if isempty(smpl)
                    warning('Broken sample!')
                    break;
                end
                N = size(smpl,1);
                % Z axis is along the limbs, which corresponds to the
                % Shimmer3 local frame axis "Y".
                % R "axis" is the cylindric counterpart combining the
                % local Shimmer3 X and Z axis.
                %
                % Quaternion gives the rotation in the NED frame
                accMagZ = zeros(N,1);
%                 accMagR = zeros(N,1);
%                 rotRateZ = zeros(N,1);
%                 rotRateR = zeros(N,1);
                groundAngleZ = zeros(N,1);
                for i = 1:N
                    % rotate ACC X*Y*Z* data into lowNoiseACC Coordinate system
                    Q = smpl(i,1:4)';

                    accInShimCoords = qRotatePoint(qRotatePoint(smpl(i,8:10)',Q) - [0;0;9.81],qInv(Q));
                    accMagZ(i) = abs(accInShimCoords(2));
%                     accMagR(i) = norm(accInShimCoords) - accMagZ(i);

%                     rotRateInShimCoords = qRotatePoint(smpl(i,5:7)',qInv(Q));
%                     rotRateZ(i) = abs(rotRateInShimCoords(2));
%                     rotRateR(i) = norm(rotRateInShimCoords) - rotRateZ(i);

                    % we want to know the absolute angle of the local
                    % Y-axis versus ground
                    yvec = qRotatePoint([0;1;0],Q);
                    groundAngleZ(i) = 180*acos(abs(yvec(3)))/pi;
                end
                s = s+1
%                 imdb.images.data{s} = cat(3,accMagZ,accMagR,rotRateZ,rotRateR,groundAngleZ);
                imdb.images.data{s} = cat(3,accMagZ,groundAngleZ);
                imdb.images.labels(1,1,1,s) = label;
                imdb.meta.info(s).subj = iSubj;
                imdb.meta.info(s).sheet = sheet;
                imdb.meta.info(s).seqType = seqTypes{relevant_sequences(iSeq)};
            end
        end
    end
    % save imdb
    save(fullfile(root,sprintf('imdb_parsed_shimmer_number_%d_onlyMagZandAngle.mat',relevant_shimmer_indices(iShim))),'imdb');
end
%% visualize processed IMU features
% evaluate the big values
figure; imagesc(reshape(imdb.images.data{s},[],5));

% visualize clonic right arm, Person 1, sheet 1
smpl = parsed_shimmer_data{1}{5}{1}{1};
% smpl = parsed_shimmer_data{iShim}{iSeq}{iSubj}{sheet};
% version right... Shimmer 4! (trunk)
smpl = parsed_shimmer_data{4}{1}{1}{1};
N = size(smpl,1);
accMagZ = zeros(N,1);
accMagR = zeros(N,1);
rotRateZ = zeros(N,1);
rotRateR = zeros(N,1);
groundAngleZ = zeros(N,1);
for i = 1:N
    Q = smpl(i,1:4)';
    accInShimCoords = qRotatePoint(smpl(i,8:10)',qInv(Q));
    accMagZ(i) = abs(accInShimCoords(2));
    accMagR(i) = norm(accInShimCoords) - accMagZ(i);
    rotRateInShimCoords = qRotatePoint(smpl(i,5:7)',qInv(Q));
    rotRateZ(i) = abs(rotRateInShimCoords(2));
    rotRateR(i) = norm(rotRateInShimCoords) - rotRateZ(i);
    yvec = qRotatePoint([0;1;0],Q);
    groundAngleZ(i) = 180*acos(abs(yvec(3)))/pi;
end
figure;subplot(121); imagesc(reshape(cat(3,accMagZ,accMagR,rotRateZ,rotRateR,groundAngleZ),[],5));
subplot(122); hold on
plot(accMagZ);
plot(accMagR);
plot(rotRateZ);
plot(rotRateR);
plot(groundAngleZ);
hold off
legend('accMagZ','accMagR','rotRateZ','rotRateR','groundAngle')
% it appears that the rotation rate is very noisy!

%% train network

clear opts
% opts.train.learningRate = [
%     ones(1,60)*0.005...
%     ones(1,100)*0.004...
%     ones(1,100)*0.002...
%     0.001
%     ];
opts.train.learningRate = [1e-2];
opts.train.gpus = 1;
opts.train.numEpochs = 1000 ;
opts.train.batchSize = 100 ;
opts.train.momentum = 0.9 ;
testSubj = 1;
for iShim = 1:4
    opts.imdbPath = fullfile(root,sprintf('imdb_parsed_shimmer_number_%d_onlyMagZandAngle.mat',relevant_shimmer_indices(iShim)));
%     for testSubj = 1:5
    [net_bn, info_bn] = train_MLP(opts,'expDir',...
        fullfile(root,sprintf('DNN_1DConv_3Layers_testSubj%d_shimmerNumber_%d_stretchAugWdw200Ratio02_lr_1e-2_2channels_momentum9_meanSdevNorm',testSubj,relevant_shimmer_indices(iShim))),...
        'batchNormalization', false, 'validationSubject', testSubj, 'numOutputs', numel(seqPerRelevantShimmer{iShim}));
%     end
end

%% visualize results
% weights
figure;
for i = 1:64
    subplot(8,8,i)
    imagesc(squeeze(net.layers{1}.weights{1}(:,1,:,i)));
end

% estimation on a real sequence
load(opts.imdbPath)
testSamples = find([imdb.meta.info.subj] == 1);

hypermotTestSample = testSamples(find(...
    strcmp({imdb.meta.info(testSamples).seqType},'VersionLeft_So4LeftStretched_GTKA'),1,'last'));
smpl = imdb.images.data{hypermotTestSample};

test_net = vl_simplenn_move(net,'gpu');
test_net.layers = test_net.layers(1:(end-1));
test_net.layers{end+1} = struct('type', 'softmax') ;

wdw = 200;
predictions = [];
for o = 0:(size(smpl,1)-wdw)
    input = single(smpl(o+(1:wdw),:,[1 2]));
    input = bsxfun(@rdivide,bsxfun(@minus,input,net.meta.average),net.meta.sdev);
    res = vl_simplenn(test_net,gpuArray(imresize(input,[100 1])),[],[],'mode','test');
%     res = vl_simplenn(test_net,gpuArray(input));
    predictions(:,o+1) = squeeze(gather(res(end).x));
end

figure;
subplot(2,1,1)
plot(predictions');
legend(seqTypes(relevant_sequences(seqPerRelevantShimmer{iShim})))
subplot(2,1,2)
plot(squeeze(smpl(wdw/2:(end-wdw/2),:,[1 2])))
figure
plot(squeeze(smpl(:,:,[1 2])))

%% test on real patient data
%IM1307: Seizure@
%IM1308: Seizure_1@March25th2016, 08:03:14

patRoot = 'E:\Shimmer Data\IM1307_2016-03-23_15.19.16_2016_03_23_SD_Session1';% patRoot = 'E:\Shimmer Data\IM1308_2016-03-24_16.16.31_2016_03_22_SD_Session2';
shimmerNames = {'1','2','4','5'}; %{'10a','12a','13a','14a'};
limbNames = {'rightWrist','leftWrist','leftAnkle','abdomen'};
sourceFn = '2016_03_23_Session1_Shimmer_%s_Calibrated_SD.csv'; %'2016_03_22_Session2_Shimmer_%s_Calibrated_SD.csv';
seizureStartTime = datenum('2016/03/23 18:59:18');
patientID = 'IM1307';
seizureID = '02';
mapping_from_Test_to_Training(1) = 1;
mapping_from_Test_to_Training(2) = 1;
mapping_from_Test_to_Training(3) = 7;
mapping_from_Test_to_Training(4) = 9;

wdw = 200;
accMagZ = zeros(wdw,1);
groundAngleZ = zeros(wdw,1);
testBatchSize = 10*52; %test 1 minute at once
beforeAfterSeiure = datenum(0,0,0,0,5,0);

% TS: [1]
params.ts = 1;
% Acc: [2-4]
params.Acc = 2:4;
% Gyro: [5-7]
% Mag: [8-10]
% Quat: [11-14]
params.Quat = 11:14;
    
for iShim = 1:numel(shimmerNames)
    tic
    % load network
    load(sprintf('DNN_1DConv_3Layers_testSubj%d_shimmerNumber_%d_stretchAugWdw200Ratio02_lr_1e-2_2channels_momentum9_meanSdevNorm.mat',1,mapping_from_Test_to_Training(iShim)))
    test_net = vl_simplenn_move(net,'gpu');
    test_net.layers{end} = struct('type', 'softmax') ;

    predictions = single([]);
    timestamps = cell(1);
    
    filename = sprintf(sourceFn,shimmerNames{iShim});
    shiF = fopen(fullfile(patRoot,filename));
    firstRow = fgetl(shiF); % gives away the data separator
    secondRow = fgetl(shiF);
    dataTypes = regexp(secondRow,'\t','split');
    thirdRow = fgetl(shiF);
    unitNames = regexp(thirdRow,'\t','split');

    splitName = regexp(dataTypes{1},'_','split');
    params.sensorIDs = str2double(splitName{2});

    i = 1;
    initDone = false;
    jumpInMinutes = true;
    isFirstRow = true;
    testBatch = [];
    while 1
        if jumpInMinutes
            for iL = 1:(60*51.2) %1 minute steps
                tmp = fgetl(shiF);
            end
        else
            tmp = fgetl(shiF);
        end
        if ~ischar(tmp), break, end
        
        dataRow = regexp(tmp,'\t','split');
        
        ts = datenum(dataRow{params.ts});
        if isFirstRow
            firstTs = ts;
            isFirstRow = false;
        end
        if ~initDone
            disp(dataRow{params.ts})
        end
        if ts < seizureStartTime-beforeAfterSeiure, continue; end
        if ts > seizureStartTime+beforeAfterSeiure, break; end
        
        disp('Starting to collect data for classification...')
        
        jumpInMinutes = false;
        Q = str2double(dataRow(params.Quat))';

        acc = str2double(dataRow(params.Acc))';
        accInShimCoords = qRotatePoint(qRotatePoint(acc,Q) - [0;0;1],qInv(Q));
        yvec = qRotatePoint([0;1;0],Q);
        
        if ~initDone
            accMagZ(i) = abs(accInShimCoords(2))*9.81;
            groundAngleZ(i) = 180*acos(abs(yvec(3)))/pi;
            i = i+1;
            initDone = i == 201;
            if initDone
                i = 1;
            end
        end
        
        if initDone
            % update input
            accMagZ = [accMagZ(2:end); abs(accInShimCoords(2))*9.81];
            groundAngleZ = [groundAngleZ(2:end); 180*acos(abs(yvec(3)))/pi];
            input = single(cat(3,accMagZ,groundAngleZ));
            input = bsxfun(@rdivide,bsxfun(@minus,input,net.meta.average),net.meta.sdev);
            if ~isempty(testBatch)
                testBatch = cat(4,testBatch,input);
            else
                testBatch = input;
            end
            timestamps{i} = dataRow{params.ts};
            i = i+1;
            if mod(i,testBatchSize) == 0
                % make estimation
                res = vl_simplenn(test_net,...
                    gpuArray(imresize(testBatch,[100 1])),...
                    [],[],'mode','test');
                if ~isempty(predictions)
                    predictions = ...
                        cat(2,predictions,single(squeeze(gather(res(end).x))));
                else
                    predictions = ...
                        single(squeeze(gather(res(end).x)));
                end
                fprintf('[%s] Probability for GTKA: %5.2f\n',dataRow{1},predictions(1,i-1)*100)
                testBatch = [];
            end
        end
    end
    fclose(shiF);

    % estimation on last (leftover) batch
    if ~isempty(testBatch)
        res = vl_simplenn(test_net,...
            gpuArray(imresize(testBatch,[100 1])),...
            [],[],'mode','test');
        if ~isempty(predictions)
            predictions = ...
                cat(2,predictions,single(squeeze(gather(res(end).x))));
        else
            predictions = ...
                single(squeeze(gather(res(end).x)));
        end
        fprintf('[%s] Probability for GTKA: %5.2f\n',dataRow{1},predictions(1,i-1)*100)
        testBatch = [];
    end

    figure; plot(predictions')
    legend(seqTypes(relevant_sequences(seqPerRelevantShimmer{...
        relevant_shimmer_indices==mapping_from_Test_to_Training(iShim)...
        })));
    % modify ticks
    set(gca,'xtick',1:(5*51.2):size(predictions,2)) %locations
    set(gca,'xticklabel',datestr(timestamps(1:(5*51.2):size(predictions,2)),13)) %labels
    title(['Classification on ' limbNames{iShim}])
    drawnow

    % save
    save(sprintf('SemiologyClassification_patient_%s_seizure_%s_shimmer_%s_%s.mat',...
        patientID,seizureID,shimmerNames{iShim},limbNames{iShim}),...
        'predictions','timestamps');

    fprintf('\nTime to\nbrowse %d hours,\nclassify %d hour, %d minutes,\nand save the result: %s\n',...
        str2double(datestr(datenum(timestamps{end})-firstTs,'HH')),...
        str2double(datestr(beforeAfterSeiure*2,'HH')),str2double(datestr(beforeAfterSeiure*2,'MM')),...
        datestr(datenum([0,0,0,0,0,toc]),13));
end

%% combine estimations of different Shimmers!
initializeShimmerClassification

patRoot = 'E:\Shimmer Data\IM1307_2016-03-23_15.19.16_2016_03_23_SD_Session1';% patRoot = 'E:\Shimmer Data\IM1308_2016-03-24_16.16.31_2016_03_22_SD_Session2';
sourceFn = '2016_03_23_Session1_Shimmer_%s_Calibrated_SD.csv'; %'2016_03_22_Session2_Shimmer_%s_Calibrated_SD.csv';
patientID = 'IM1307';

shimmerNames = {'1','2','4','5'}; %{'10a','12a','13a','14a'};
limbNames = {'rightWrist','leftWrist','leftAnkle','abdomen'};

mapping_from_Test_to_Training(1) = 1;
mapping_from_Test_to_Training(2) = 1;
mapping_from_Test_to_Training(3) = 7;
mapping_from_Test_to_Training(4) = 9;

seizureID = '02';
seizureStartTime = datenum('2016/03/23 18:59:18');

for iShim = 1:numel(shimmerNames)
    % load estimation
    load(sprintf('SemiologyClassification_patient_%s_seizure_%s_shimmer_%s_%s.mat',...
    patientID,seizureID,shimmerNames{iShim},limbNames{iShim}),...
    'predictions','timestamps');

    ts_num{iShim} = datenum(timestamps);
    pred{iShim} = predictions;
end

% Shimmer data are synchronized using nearest neighbor timestamps
% [the Shimmer with the lowest ID was the Master, so it will serve as the
% host for the "correct" timestamps
numMasterTs = numel(ts_num{1});
syncedIndices = zeros(numel(shimmerNames),numMasterTs);
syncedIndices(1,:) = 1:numMasterTs;
for iT = 1:numMasterTs
    cTs = ts_num{1}(iT);
    for iShim = 2:numel(shimmerNames)
        [~,nnInd] = min(abs(ts_num{iShim}-cTs));
        syncedIndices(iShim,iT) = nnInd;
    end
end

% debug plot
figure; plot(syncedIndices'); legend(shimmerNames); axis image

% find out how many classes there are in total
allClasses = unique(relevant_sequences([seqPerRelevantShimmer{:}]));
disp(seqTypes(allClasses))









