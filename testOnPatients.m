%% test on real patient data
% TODO:
% - add a flag for UnixTimestamps in the exported data
% - make this work with old exported Shimmer Data from MSS
% - add a synchronization function (manually), by checking the Kinect Video
% at the seizure event and comparing accelerations

%IM1307: Seizure_2@March23rd2016, 18:59:18
%IM1308: Seizure_1@March25th2016, 08:03:14
initializeShimmerClassification
test_eventID = 6;
networkDir = 'C:\Projects\SeizureClassification\Accelerometers\networks';
resultsDir = 'C:\Projects\SeizureClassification\Results';

% download updated .csv list from GoogleDocs
seizureList = GetGoogleSpreadsheet('19wIL-j6geMGvy_pABonnsiyqWk2MTE56MnOU_AYhMfA');
% page 2: 19wIL-j6geMGvy_pABonnsiyqWk2MTE56MnOU_AYhMfA/edit#gid=1018055961
seizureTable = cell2table(seizureList(2:end,:),'VariableNames',cell(seizureList(1,:)));
test_row = find(str2double(seizureTable{:,'event_ID'}) == test_eventID);

% check if the .csv files are in an unusual format
manualExport = false;
if strcmp('manualExport',seizureTable{test_row,'notes'})
    manualExport = true;
end

fullPatientPath = seizureTable{test_row,'filepath_shimmer'};
[patRoot,sourceFn,ext]=fileparts(fullPatientPath{1}); %patRoot = 'E:\Shimmer Data\exported_consensys\IM1307_2016-03-23_15.19.16_2016_03_23_SD_Session1';% patRoot = 'E:\Shimmer Data\IM1308_2016-03-24_16.16.31_2016_03_22_SD_Session2';
shimmerNames = seizureTable{test_row,...
    {'shimNameRARM', 'shimNameLARM', 'shimNameRLEG', 'shimNameLLEG', 'shimNameHIP'}...
    }; %{'1','2','4','5'}; %{'10a','12a','13a','14a'} for IM1308
invalidShimmers = strcmp(shimmerNames,'X');
shimmerNames(invalidShimmers) = [];
limbNames = {'rightWrist','leftWrist','rightAnkle','leftAnkle','abdomen'};
limbNames(invalidShimmers) = [];
sourceFn = [sourceFn ext];%sourceFn = '2016_03_23_Session1_Shimmer_%s_Calibrated_SD.csv'; %'2016_03_22_Session2_Shimmer_%s_Calibrated_SD.csv';
seizureStartTime = datenum(seizureTable{test_row,'clin_start'});
patientID = seizureTable{test_row,'Patient_ID'};%'IM1307';
patientID = patientID{1};
seizureID = sprintf('%02i',str2double(seizureTable{test_row,'sz_number'}));%'02';

mapping_from_TestShimID_to_TrainingShimName = zeros(numel(limbNames),1);
for iShim = 1:numel(shimmerNames)
    switch limbNames{iShim}
        case 'rightWrist'
            mapping_from_TestShimID_to_TrainingShimName(iShim) = 1;
        case 'leftWrist'
            mapping_from_TestShimID_to_TrainingShimName(iShim) = 1;
        case 'rightAnkle'
            mapping_from_TestShimID_to_TrainingShimName(iShim) = 5;
        case 'leftAnkle'
            mapping_from_TestShimID_to_TrainingShimName(iShim) = 7;
        case 'abdomen'
            mapping_from_TestShimID_to_TrainingShimName(iShim) = 9;
    end
end

if manualExport
    configFilePath = fullfile(patRoot,'Shimmers_Config.csv');
    confF = fopen(configFilePath);
    firstRow = fgetl(confF);
    timeoffset = zeros(1,numel(shimmerNames));
    configtime = timeoffset;
    for iShim = 1:numel(shimmerNames)
        tmp = fgetl(confF);
        dataRow = regexp(tmp,';','split');
        timeoffset(iShim) = str2double(dataRow{end});
        configtime(iShim) = str2double(dataRow{40});
    end
    fclose(confF);
end
    
wdw = 200;
accMagZ = zeros(wdw,1);
groundAngleZ = zeros(wdw,1);
testBatchSize = 60*51.2; %test 1 minute at once
beforeAfterSeiure = datenum(0,0,0,0,5,0); % report results for 5 minutes before and after the seizure

% Define the columns in the .CSV files
if manualExport
    % TS: [1]
    params.ts = 3;
    % Acc: [2-4]
    params.Acc = 11:13;
    % Gyro: [5-7]
    % Mag: [8-10]
    % Quat: [11-14]
    params.Quat = 4:7;
else
    % TS: [1]
    params.ts = 1;
    % Acc: [2-4]
    params.Acc = 2:4;
    % Gyro: [5-7]
    % Mag: [8-10]
    % Quat: [11-14]
    params.Quat = 11:14;
end
    
for iShim = 1:numel(shimmerNames)
    tic
    % load network
    load(fullfile(networkDir,...
        sprintf('DNN_1DConv_3Layers_testSubj%d_shimmerNumber_%d_stretchAugWdw200Ratio02_lr_1e-2_2channels_momentum9_meanSdevNorm.mat',...
        1,mapping_from_TestShimID_to_TrainingShimName(iShim))));
    test_net = vl_simplenn_move(net,'gpu');
    test_net.layers{end} = struct('type', 'softmax') ;

    predictions = single([]);
    timestamps = cell(1);
    features = cell(1);
    
    filename = sprintf(sourceFn,shimmerNames{iShim});
    shiF = fopen(fullfile(patRoot,filename));
    firstRow = fgetl(shiF); % gives away the data separator
    if ~manualExport
        secondRow = fgetl(shiF);
        dataTypes = regexp(secondRow,'\t','split');
        thirdRow = fgetl(shiF);
        unitNames = regexp(thirdRow,'\t','split');

        splitName = regexp(dataTypes{1},'_','split');
        params.sensorIDs = str2double(splitName{2});
    end

    i = 1;
    initDone = false;
    jumpInMinutes = true;
    isFirstRow = true;
    testBatch = [];
    eofBeforeSeizure = false;
    UNIXmilliseconds = false;
    if strcmp('yes',seizureTable{test_row,'unixTSonly'})
        UNIXmilliseconds = true;
    end
    while 1
        try
            if jumpInMinutes
                for iL = 1:(60*51.2) %1 minute steps
                    tmp = fgetl(shiF);
                end
            else
                tmp = fgetl(shiF);
            end
        catch e
            fprintf('Read error at Shimmer %s (%s)!\nprobably: file end before seizure begin\n',...
                shimmerNames{iShim},limbNames{iShim});
            eofBeforeSeizure = true;
            break;
        end
        
        if ~ischar(tmp)
            fprintf('Read error at Shimmer %s (%s)!\nprobably: file end before seizure end\n',...
                shimmerNames{iShim},limbNames{iShim});
            eofBeforeSeizure = true;
            break;
        end
        
        dataRow = regexp(tmp,'\t','split');
        
        if UNIXmilliseconds
            ts = datetime(str2double(dataRow{params.ts})/1000, 'ConvertFrom', 'posixtime');
            ts = datenum(ts);
            if isnan(ts)
                eofBeforeSeizure = true;
                break;
            end
        elseif manualExport
            % TODO: cover this condition
            dataRow = regexp(tmp,',','split');
            ts = datetime(str2double(dataRow{params.ts})/1000 + timeoffset(iShim)/1000 + configtime(iShim), 'ConvertFrom', 'posixtime');
            ts = datenum(ts);
        else
            ts = datenum(dataRow{params.ts});
        end
        if isFirstRow
            firstTs = ts;
            isFirstRow = false;
        end
        if ~initDone && jumpInMinutes
            disp(datestr(ts))
        end
        if ts < seizureStartTime-beforeAfterSeiure, continue; end
        if ts > seizureStartTime+beforeAfterSeiure, break; end
        
        if i==1
            disp('Starting to collect data for classification...');
        end
        
        jumpInMinutes = false;
        Q = str2double(dataRow(params.Quat))';

        acc = str2double(dataRow(params.Acc))';
        accInShimCoords = qRotatePoint(qRotatePoint(acc,Q) - [0;0;1],qInv(Q));
        yvec = qRotatePoint([0;1;0],Q);
        
        if ~initDone
            accMagZ(i) = abs(accInShimCoords(2))*9.81;
            groundAngleZ(i) = 180*acos(abs(yvec(3)))/pi;
            i = i+1;
            initDone = i == wdw+1;
            if initDone
                i = 1;
            end
        end
        
        if initDone
            % update input
            features{i} = [accMagZ(1);groundAngleZ(1)];
            accMagZ = [accMagZ(2:end); abs(accInShimCoords(2))*9.81];
            groundAngleZ = [groundAngleZ(2:end); 180*acos(abs(yvec(3)))/pi];
            input = single(cat(3,accMagZ,groundAngleZ));
            input = bsxfun(@rdivide,bsxfun(@minus,input,net.meta.average),net.meta.sdev);
            if ~isempty(testBatch)
                testBatch = cat(4,testBatch,input);
            else
                testBatch = input;
            end
            timestamps{i} = datestr(ts);
            i = i+1;
            if mod(i,testBatchSize) == 0
                % make estimation every "testBatchSize" patches
                res = vl_simplenn(test_net,...
                    gpuArray(imresize(testBatch,[100 1])),...
                    [],[],'mode','test');
                if ~isempty(predictions)
                    predictions = ...
                        cat(2,predictions,single(squeeze(gather(res(end).x))));
                else
                    predictions = ...
                        single(squeeze(gather(res(end).x)));
                end
                fprintf('[%s] Probability for GTKA: %5.2f\n',datestr(ts),predictions(1,i-1)*100)
                testBatch = [];
            end
        end
    end
    fclose(shiF);
    if eofBeforeSeizure
        continue;
    end
    % estimation on last (leftover) batch
    if ~isempty(testBatch)
        res = vl_simplenn(test_net,...
            gpuArray(imresize(testBatch,[100 1])),...
            [],[],'mode','test');
        if ~isempty(predictions)
            predictions = ...
                cat(2,predictions,single(squeeze(gather(res(end).x))));
        else
            predictions = ...
                single(squeeze(gather(res(end).x)));
        end
        fprintf('[%s] Probability for GTKA: %5.2f\n',dataRow{1},predictions(1,i-1)*100)
    end

    figure;
    subplot(211)
    plot(predictions')
    legend(seqTypes(relevant_sequences(seqPerRelevantShimmer{...
        relevant_shimmer_indices==mapping_from_TestShimID_to_TrainingShimName(iShim)...
        })));
    % modify ticks, replace sample numbers by Timestamps
    set(gca,'xtick',1:(5*51.2):size(predictions,2)) %locations
    set(gca,'xticklabel',datestr(timestamps(1:(5*51.2):size(predictions,2)),13)) %labels
    set(gca,'xticklabelrotation',90) %labels
    title(['Classification on ' limbNames{iShim}])
    subplot(212)
        plot([features{:}]')
    legend('accMagZ','groundAngleZ');
    % modify ticks, replace sample numbers by Timestamps
    set(gca,'xtick',1:(5*51.2):size(predictions,2)) %locations
    set(gca,'xticklabel',datestr(timestamps(1:(5*51.2):size(predictions,2)),13)) %labels
    set(gca,'xticklabelrotation',90) %labels
    title(['Features of ' limbNames{iShim}])
    
    set(gcf,'position',[1,1,1920,916])
    drawnow

    % save result
    save(fullfile(resultsDir,...
        sprintf('SemiologyClassification_patient_%s_seizure_%s_shimmer_%s_%s.mat',...
        patientID,seizureID,shimmerNames{iShim},limbNames{iShim})),...
        'predictions','timestamps','features');
    print(fullfile(resultsDir,...
        sprintf('SemiologyClassification_patient_%s_seizure_%s_shimmer_%s_%s.png',...
        patientID,seizureID,shimmerNames{iShim},limbNames{iShim})),'-dpng');

    fprintf('\nTime to\nbrowse %d hours,\nclassify %d hour, %d minutes,\nand save the result: %s\n',...
        str2double(datestr(datenum(timestamps{end})-firstTs,'HH')),...
        str2double(datestr(beforeAfterSeiure*2,'HH')),str2double(datestr(beforeAfterSeiure*2,'MM')),...
        datestr(datenum([0,0,0,0,0,toc]),13));
end