
%% Body Model with Limb Indices
% % arms (leftLower, leftUpper, rightLower, rightUpper)
% bodyModel.limbs{1} = makeLimbStruct(250,[0 1 0]');
% bodyModel.limbs{2} = makeLimbStruct(250,[0 1 0]');
% bodyModel.limbs{3} = makeLimbStruct(250,[0 -1 0]');
% bodyModel.limbs{4} = makeLimbStruct(250,[0 -1 0]');
% % legs (leftLower, leftUpper, rightLower, rightUpper)
% bodyModel.limbs{5} = makeLimbStruct(400,[0 1 0]');
% bodyModel.limbs{6} = makeLimbStruct(400,[0 1 0]');
% bodyModel.limbs{7} = makeLimbStruct(400,[0 -1 0]');
% bodyModel.limbs{8} = makeLimbStruct(400,[0 -1 0]');
% 
% % head
% bodyModel.limbs{9} = makeLimbStruct(220);
% % upper abdomen
% bodyModel.limbs{10} = makeLimbStruct(600);

% % Sensor->Limb correspondences
% % the n-th shimmer is attached to this respective bodypart!
% %         bodyModel.params.sensAt(n) = bdyPtIdx;
% bodyModel.params.sensAt(1) = 3;
% bodyModel.params.sensAt(2) = 4;
% bodyModel.params.sensAt(3) = 1;
% bodyModel.params.sensAt(4) = 2;
% bodyModel.params.sensAt(5) = 7;
% bodyModel.params.sensAt(6) = 8;
% bodyModel.params.sensAt(7) = 5;
% bodyModel.params.sensAt(8) = 6;
% bodyModel.params.sensAt(9) = 10;
% bodyModel.params.sensAt(10)= 9;

% Shimmer Sensor Names (indices/IDs) during our Simulation Recordings
% [1 2 3 4 5 6 7 8 9 11]

%% 
rootDir = 'E:\ShimmerData_GA\SeizureSimulations_June2015';

shimmerAtLimb.LRA = 1;
shimmerAtLimb.URA = 2;
shimmerAtLimb.LLA = 3;
shimmerAtLimb.ULA = 4;
% shimmerAtLimb.
% ...
% This struct lists all shimmer (indices? names?) per body part

shimmerIDtoLimbName{1} = 'RightArm';
shimmerIDtoLimbName{5} = 'RightLeg';
shimmerIDtoLimbName{7} = 'LeftLeg';
shimmerIDtoLimbName{9} = 'Torso';

relevant_shimmer_indices = [1 3 5 7 9]; % this would mean I have to handle the
% missing left arm in the Monday_61min recording (Jan Remi & Oliver Klein)
% relevant_shimmer_indices = [1 5 7 9];

% seqTypes = {'VersionRight_So4RightStretched_GTKA','VersionLeft_So4LeftStretched_GTKA',...
%     'AutomatismsBothHands','OralAutomatisms','Hypermotor',...
%     'ClonicRightArm','ClonicLeftArm','TonicBothSides'};
seqTypes = {...
    'VersionRight_So4','RightStretched_GTKA',...
    'VersionLeft_So4','LeftStretched_GTKA',...
    'AutomatismsBothHands','OralAutomatisms','Hypermotor',...
    'ClonicRightArm','ClonicLeftArm','TonicBothSides'};

% all except oral automatisms
% relevant_sequences = [1 2 3 5 6 7 8];

% modified relevant_sequences for splitted VersionSo4 and GTKA
relevant_sequences = [1 2 3 4 5 7 8 9 10]; % 6 == oral automatisms

% relevant sequences OF the filtered sequences, for each shimmer
seqPerRelevantShimmer = {
    [2 3 4 5 7] %LwrRArm: Version Left + So4 + GTKA; autom. Hands; hypermot; clon. right; tonic both
    [1 3 4 6 7] %LwrLArm: Version Right + So4 + GTKA; autom. Hands; hypermot; clon. left; tonic both
    [2 4 7] %LwrRLeg: Version Left + So4 + GTKA; hypermot; tonic both
    [1 4 7] %LwrLLeg: Version Right + So4 + GTKA; hypermot; tonic both
    [1 2 4] %trunk: Version Right + So4 + GTKA; Version Left + So4 + GTKA; hypermot
    };

%% assign class labels
% thoughts about Shimmer Seizure classes here:
% https://docs.google.com/document/d/152DdAsV5wcs9tPGqdJ9kn2Guc405BssgM_J7zSsgNLg/edit

% with distinct GTKA direction
%  1. Normal
%  2. Hypermotor
%  3. VersionIpsi
%  4. VersionContra
%  5. GTKAIpsi
%  6. GTKAContra
%  7. TonicAll
%  # from now on for wrist-Shimmers only
%  8. ClonicHandsIpsi
%  9. AutomotorHandsIpsi
% sequenceNumberToLabel_RArm = [3 5 4 6 9 2 8 NaN 7];
% sequenceNumberToLabel_LArm = [4 6 3 5 9 2 NaN 8 7];
% sequenceNumberToLabel_RLeg = [3 5 4 6 NaN 2 NaN NaN 7];
% sequenceNumberToLabel_LLeg = [4 6 3 5 NaN 2 NaN NaN 7];
% sequenceNumberToLabel_LHip = [4 6 3 5 NaN 2 NaN NaN 7];

% without GTKA direction
labelTypes = {...
    'Neutral'
    'Hypermotor'
    'VersionIpsi'
    'VersionContra'
    'GTKA'
    'Tonic'
    'ClonicHandsIpsi'
    'AutomotorHandsIpsi'};
%  1. Normal
%  2. Hypermotor
%  3. VersionIpsi
%  4. VersionContra
%  5. GTKAAll
%  6. TonicAll
%  # from now on for wrist-Shimmers only
%  7. ClonicHandsIpsi
%  8. AutomotorHandsIpsi
sequenceNumberToLabel_RArm = [3 5 4 5 8 2 7 NaN 6];
sequenceNumberToLabel_LArm = [4 5 3 5 8 2 NaN 7 6];
sequenceNumberToLabel_RLeg = [3 5 4 5 NaN 2 NaN NaN 6];
sequenceNumberToLabel_LLeg = [4 5 3 5 NaN 2 NaN NaN 6];
sequenceNumberToLabel_LHip = [4 5 3 5 NaN 2 NaN NaN 6];
