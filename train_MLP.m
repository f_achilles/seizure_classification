function [net, info] = train_MLP(opts,varargin)
%based on "CNN_MNIST  Demonstrates MatConvNet on MNIST" template

opts.batchNormalization = false ;
opts.networkType = 'simplenn' ;
opts.validationSubject = 1;
opts.expDir = pwd;
opts.numOutputs = 7;
opts = vl_argparse(opts, varargin) ;
% 
sfx = opts.networkType ;
if opts.batchNormalization, sfx = [sfx '-bnorm'] ; end

% --------------------------------------------------------------------
%                                                         Prepare data
% --------------------------------------------------------------------

net = MLP_init('batchNormalization', opts.batchNormalization, ...
                     'networkType', opts.networkType,...
                     'numOutputs', opts.numOutputs) ;

if exist(opts.imdbPath, 'file')

else
  mkdir(opts.expDir) ;
end

imdb = load(opts.imdbPath) ;
imdb = imdb.imdb;
N = size(imdb.images.data,2);

if iscell(imdb.images.data)
    imdb.images.set = ones(1,N);
    imdb.images.set([imdb.meta.info.subj] == opts.validationSubject) = 2;

    trainIndices = find(imdb.images.set == 1);
    data = [];
    for s = 1:numel(trainIndices)
        data = cat(1,data,imdb.images.data{trainIndices(s)});
    end
    net.meta.average = mean(data,1);
    net.meta.sdev = std(data,1);

    for s=1:N
        imdb.images.data{s} = bsxfun(@rdivide,bsxfun(@minus,imdb.images.data{s},net.meta.average),net.meta.sdev);
    end
end

fully = false;
if fully
else
%     temp = reshape(imdb.images.data,400,1,4,[]);
%     temp2 = cat(3,temp(1:4:end,1,:,:),temp(2:4:end,1,:,:),temp(3:4:end,1,:,:),temp(4:4:end,1,:,:));
%     imdb.images.data = temp2;
end
% net.meta.classes.name = arrayfun(@(x)sprintf('%d',x),1:10,'UniformOutput',false) ;

% --------------------------------------------------------------------
%                                                                Train
% --------------------------------------------------------------------

opts.networkType = 'simplenn';
switch opts.networkType
  case 'simplenn', trainfn = @cnn_train ;
  case 'dagnn', trainfn = @cnn_train_dag ;
end

% [net, info] = trainfn(net, imdb, getBatch(opts), ...
%   'expDir', opts.expDir, ...
%   net.meta.trainOpts, ...
%   opts.train, ...
%   'val', find(imdb.images.set == 2)) ;
[net, info] = trainfn(net, imdb, getBatch(opts), ...
  'expDir', opts.expDir, ...
  opts.train, ...
  'val', find(imdb.images.set == 2)) ;

end

% --------------------------------------------------------------------
function fn = getBatch(opts)
% --------------------------------------------------------------------
switch lower(opts.networkType)
  case 'simplenn'
    fn = @(x,y) getSimpleNNBatch(x,y) ;
  case 'dagnn'
    bopts = struct('numGpus', numel(opts.train.gpus)) ;
    fn = @(x,y) getDagNNBatch(bopts,x,y) ;
end

end

% --------------------------------------------------------------------
function [images, labels] = getSimpleNNBatch(imdb, batch)
% --------------------------------------------------------------------
wdwSz = 200;
nCh = 2;
scaleAugmentation = false;
if scaleAugmentation
    rangeRatio = 0.2;
    range = floor(rangeRatio*wdwSz);
end
images = zeros(100,1,nCh,numel(batch),'single');
for s = 1:numel(batch)
    smplLen = size(imdb.images.data{batch(s)},1);
    if scaleAugmentation
        length = min(smplLen,wdwSz + randperm(range,1) - floor(range/2));
    else
        length = min(smplLen,wdwSz);
    end
    start = randperm(max(smplLen-length,1),1);
    images(:,:,:,s) = single(imresize(imdb.images.data{batch(s)}(start:(start+length-1),1,:),[100 1],'bilinear'));
end
% images = imdb.images.data(:,:,:,batch) ;
% flip = randi(2,4,1)==1;
% flip = repmat(flip,4,1);
% flipSamples = randi(2,numel(batch),1)==1;
% for is = find(flipSamples)'
%     images(:,:,flip,is) = -images(:,:,flip,is);
% end
labels = imdb.images.labels(1,batch) ;

% this generates a binary labelling in background (label == 1) vs. seizure (label == 2)
% labels(labels>1) = 2;
end

% --------------------------------------------------------------------
function inputs = getDagNNBatch(opts, imdb, batch)
% --------------------------------------------------------------------
images = imdb.images.data(:,:,:,batch) ;
labels = imdb.images.labels(1,batch) ;
if opts.numGpus > 0
  images = gpuArray(images) ;
end
inputs = {'input', images, 'label', labels} ;

end